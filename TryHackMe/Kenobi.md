# Task 1
Deploy and scan the machine with nmap 

`nmap -sV -sC <ip>`

Q: Scan the machine with nmap, how many ports are open?
A 7

# Task 2

Using nmap we can enumerate a machine for SMB shares.

Nmap has the ability to run to automate a wide variety of networking tasks. There is a script to enumerate shares!

`nmap -p 445 --script=smb-enum-shares.nse,smb-enum-users.nse 10.10.53.75`

SMB has two ports, 445 and 139.

Port 139: SMB originally ran on top of NetBIOS using port 139. NetBIOS is an older transport layer that allows Windows computers to talk
to each other on the same networking

Port 445: Later versios of SMB (after Windows 2000) began t ouse port 445 on top of a TCP stack. Using TCP allows SMB to work over the Internet

Q: Using the nmap command above, how many shares have been found?
A: 3

![kenobi](/img/kenobi.png)


On most distributions of Linux smbclient is already installed. Lets inspect one of the shares.

`smbclient //<ip>/anonymous`

Using your machine, connect to the machines network share.

Q: Once you're connected, list the files on the share. What is the file can you see?

![kenobi2](/img/kenobi2.png)

A: log.txt

Run CMD `exit` to get out of the smb shell
Run `smbget -R smb://<ip>/anonymous`, press ENTER (blank passsword)

Once the shares are downloaded, run `cat log.txt`

![kenobi3](/img/kenobi3.png)



Your earlier nmap port scan will have shown port 111 running the service rpcbind. This is just an server that converts remote procedure call (RPC) program number into universal addresses. When an RPC service is started, it tells rpcbind the address at which it is listening and the RPC program number its prepared to serve. 

In our case, port 111 is access to a network file system. Lets use nmap to enumerate this.
RUn `nmap -p 111 --script=nfs-ls,nfs-statfs,nfs-showmount 10.10.53.75`

Q: What mount can we see?
![kenobi4](/img/kenobi4.png)

# Task 3:: Gain intial access with ProFtpd
ProFtpd is a free and open-source FTP server, compatible with Unix and Windows systems. Its also been vulnerable in the past software versions.

Q: What is the version?

Run `nc <ip> 21`
![kenobi5](/img/kenobi5.png)

A: 1.3.5



We can use searchsploit to find exploits for a particular software version.

Searchsploit is basically just a command line search tool for exploit-db.com.

Q: How many exploits are there for the ProFTPd running?
![kenobi6](/img/kenobi6.png)

A: 3



You should have found an exploit from ProFtpd's mod_copy module. 

The mod_copy module implements SITE CPFR and SITE CPTO commands, which can be used to copy files/directories from one place to another on the server. Any unauthenticated client can leverage these commands to copy files from any part of the filesystem to a chosen destination.

We know that the FTP service is running as the Kenobi user (from the file on the share) and an ssh key is generated for that user. 
![kenobi7](/img/kenobi7.png)


We knew that the /var directory was a mount we could see (task 2, question 4). So we've now moved Kenobi's private key to the /var/tmp directory.


Lets mount the /var/tmp directory to our machine

RUn `mkdir /mnt/kenobiNFS`
`mount machine_ip:/var /mnt/kenobiNFS`
`ls -la /mnt/kenobiNFS`
![kenobi8](/img/kenobi8.png)

Login Successfully!

run `cat /home/kenobi/user.txt` to get the flag

# Task 4 - Privilege Escalation with Path Variable Manipulation

To search the a system for these type of files run the following: `find / -perm -u=s -type f 2>/dev/null`

Q: What file looks particularly out of the ordinary? 

![kenobi10](/img/kenobi10.png)

A: /usr/bin/menu

Q: RUn the binary, how many options appear?
![kenobi11](/img/kenobi11.png)

A: 3

trings is a command on Linux that looks for human readable strings on a binary.

This shows us the binary is running without a full path (e.g. not using /usr/bin/curl or /usr/bin/uname).

As this file runs as the root users privileges, we can manipulate our path gain a root shell.

We copied the /bin/sh shell, called it curl, gave it the correct permissions and then put its location in our path. This meant that when the /usr/bin/menu binary was run, its using our path variable to find the "curl" binary.. Which is actually a version of /usr/sh, as well as this file being run as root it runs our shell as root!

Q: What is the root flag (/root/root.txt)?

![kenobi12](/img/kenobi12.png)

A: Run `cat root.txt` when in root to get the flag

END





