# Task 1 - Introduction

## Renaissance

IP : 10.10.0.75

nginx 1.19.6

using Nuxt and Docker 

Found 4 Javascript files running as script

Run Dirbuster and found `http://10.10.0.75/flags.php`

Run nmap to check for open ports `nmap -sV -sC <ip>`

```
Not shown: 998 closed ports
PORT   STATE SERVICE VERSION
22/tcp open  ssh?
| fingerprint-strings: 
|   GenericLines: 
|_    d&FYg1}9R-/~];wO|1moPpM?
|_ssh-hostkey: ERROR: Script execution failed (use -d to debug)
80/tcp open  http    nginx 1.19.6
| http-robots.txt: 3 disallowed entries 
|_/api/ /exif-util /*.bak.txt$
|_http-server-header: nginx/1.19.6
|_http-title: docker-escape-nuxt 
|_http-trane-info: Problem with XML parsing of /evox/about

```

Running nmap wth tag -d to debug

Port 22 is open, running hydra with ssh

`hydra -l admin -P /usr/share/wordlists/rockyou.txt ssh://10.10.0.75`

 > got a timeout error


Run Gobuster with common.txt 

`gobuster dir -u http://10.10.0.75/ -w /usr/share/wordlists/dirb/common.txt -t 50 --wildcard
`

Results: 

```
===============================================================
2021/02/14 16:45:16 Starting gobuster
===============================================================
/_conf (Status: 200)
/.bash_history (Status: 200)
/.hta (Status: 200)
/_dev (Status: 200)
/.git/HEAD (Status: 200)
/.svn/entries (Status: 200)
/.mysql_history (Status: 200)
/.svn (Status: 200)
/.swf (Status: 200)
/_css (Status: 200)
/_config (Status: 200)
/api (Status: 301)
/.listings (Status: 200)
/.history (Status: 200)
/.perf (Status: 200)
/.forward (Status: 200)
/.htpasswd (Status: 200)
/.listing (Status: 200)
/AppsLocalLogin (Status: 200)
/contact-us (Status: 200)
/films (Status: 200)
/jsps (Status: 200)
/option (Status: 200)
/registrations (Status: 200)
/svr (Status: 200)
/wp-icludes (Status: 200)

```


Immune to SQL Injection

`/api (Status: 301)` > check the URL `<ip>/api`

`http://10.10.244.29/exif-util/` has something we can work with. We can upload file from machine or from URL 

Testing with an HTML file from machine returns 503 error

## Set up a URL with a photo to make sure that tha request comes thru

1) Get any image > I got `blue.png`> cp to /thegreatescape/uploads folder
2) run `python3 -m http.server` > this will run an HTTP server with my IP
3) navigate to `<my-ip>/blue.png` > your picture should be there
4) Navigate to `http://10.10.244.29/api/exif?url=http://10.6.31.49:8000/blue.png`
	- that first IP is the machine, the second one is mine

Returns 200 code! > That's good 


