# ARP Traffic 

Filter by Opcode:
- request (1)
- reply (2)

Looking for only reply packets, enter in the filter text box

`arp.opcode == 2`

# ICMP Traffic 

## Type of the packet

- Type = 8 : request packet
- Type = 0 : reply packet
- Type = anything else : suspicious activity

## Details to analyze in a packet
- Time Stamp: when the ping was requested
- Data : Data string, usually random


# DNS Overview

## Analyzing DNS packets
- Query Response
- DNS-Servers Only
- UDP

If UDP 53: good
if TCP 53 : suspicious

- MAke sure we check the Transactions ID, Queries and response



# HTTP 

Identify:
- Hosts
- user-agent
- requested URI 
- Response
```
Statistics > End points 
Statistics > Protocol Hierachy 
File > Export Objects > HTTP 

```

# HTTPS

## Making a secure tunnel

- Client and server agree on a protocol version
- Client and server select a cryptographic algorithm
- The client and server can authenticate to each other; this step is optional
- Creates a secure tunnel with a public key

## Client Key Exchange packet
```
Transport Layer Security
    SSLv3 Record Layer: Handshake Protocol: Server Hello
    SSLv3 Record Layer: Handshake Protocol: Certificate
    SSLv3 Record Layer: Handshake Protocol: Server Hello Done
```

## Encrypted traffic between server and client 

```
SSLv3 Record Layer: Application Data Protocol: http-over-tls
    Content Type: Application Data (23)
    Version: SSL 3.0 (0x0300)
    Length: 400
    Encrypted Application Data: 94f2532feaa7fd0cdda9dc0764762e51fc7d705b6f171d359709bacb65472254c1a5cb50…
    [Application Data Protocol: http-over-tls]

```

## Decrypt traffic

You can use an RSA key in Wireshark in order to view the data unencrypted. 

` Edit > Preferences > Protocols > TLS >  [+] `

You will need to fill in the various sections on the menu with the following preferences:


```
IP Address: 127.0.0.1

Port: start_tls

Protocol: http

Keyfile: RSA key location
```

We can now use other features in order to organize the data stream, like using the export HTTP object feature, to access this feature navigate to `File > Export Objects > HTTP`


## PCAP CVE-2020-1472
VIctim IP: 192.168.100.6
Attacker IP : 192.168.100.128

Unknown protocols: DCERPC and EPM


### Look further

Use filter to get the attacker's packets
`ip.src == 192.168.100.128`

Note: Be aware of IOCs, or Indicators of Compromise > Threat Intelligence

This is the Zerologon exploit: 
- uses multiple RPC connections
- DCERPC requests to change machine account password


See SMB2/3 and DRSUAPI traffic > exploit uses `secretdump` to dump hashes
