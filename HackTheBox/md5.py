#!/bin/usr/python

import requests
import hashlib
import re

req = requests.session()
url = "http://178.128.40.217:32503/"

## Get request

rget = req.get(url)
html = rget.content
html = html.decode('ISO-8859-1') 


 ##strip uneccessary HTML

def clean_up(html):
	clean = re.compile('<.*?>')
	return re.sub(clean, '', html)


out1 = clean_up(html)
out2 = out1.split('string')[1]
out3 = out2.rstrip()


## Encrypt md5
mdHash = hashlib.md5(out3.encode('utf-8')).hexdigest()

# Post back
data = dict(hash=mdHash)
rpost = req.post(url=url, data=data)

print(rpost.text)