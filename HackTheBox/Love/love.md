# Meta data

IP: 10.129.115.22
Date accessed: 06/21/2021

# Write up

Runing nmap for info 
- 445/tcp  open  microsoft-ds Windows 10 Pro 19042 microsoft-ds (workgroup: WORKGROUP)
- 3306/tcp open  mysql?

-running gobuster on the page

`gobuster -u http://10.129.115.22/ dir --wordlist /usr/share/wordlists/dirbuster/directory-list-2.3-medium.txt`

found a URL nmap
added this to known hosts: 
`10.129.115.22   staging.love.htb`


navigate to `http://staging.love.htb/`

Under `Demo` there's a input box for a link

port `443` is open SSL
input `http://127.0.0.1:443`
```
Bad Request

Your browser sent a request that this server could not understand.
Reason: You're speaking plain HTTP to an SSL-enabled server port.
Instead use the HTTPS scheme to access this URL, please.
Apache/2.4.46 (Win64) OpenSSL/1.1.1j PHP/7.3.27 Server at www.example.com Port 443

```

Use the HTTP instead

`http://127.0.0.1:5000`


`Vote Admin Creds admin: @LoveIsInTheAir!!!! `